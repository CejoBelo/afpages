DROP TABLE Formateur CASCADE CONSTRAINTS;
DROP TABLE Sessions CASCADE CONSTRAINTS;
DROP TABLE Formation CASCADE CONSTRAINTS;
DROP TABLE Module CASCADE CONSTRAINTS;
DROP TABLE Stagiaire CASCADE CONSTRAINTS;

 ---------------------------------------------------------------
 --        Script Oracle.  
 ---------------------------------------------------------------


------------------------------------------------------------
-- Table: Formateur
------------------------------------------------------------
CREATE TABLE Formateur(
	idFormateur      NUMBER(10,0)  NOT NULL  ,
	nomFormateur     VARCHAR2 (25) NOT NULL  ,
	prenomFormateur  VARCHAR2 (25) NOT NULL  ,
	sexeFormateur    CHAR (1)  NOT NULL  ,
	emailFormateur   VARCHAR2 (25) NOT NULL  ,
	telFormateur     VARCHAR2 (25) NOT NULL  ,
	loginFormateur   VARCHAR2 (25) NOT NULL  ,
	mdpFormateur     VARCHAR2 (25) NOT NULL  ,
	estAdmin         NUMBER(10,0) ,
	CONSTRAINT Formateur_Pk PRIMARY KEY (idFormateur) ,
	CONSTRAINT Formateur_Uniq UNIQUE (loginFormateur)
);

------------------------------------------------------------
-- Table: Sessions
------------------------------------------------------------
CREATE TABLE Sessions(
	idSession     NUMBER(10,0)  NOT NULL  ,
	nomSession    VARCHAR2 (10) NOT NULL UNIQUE ,
	debutSession  DATE  NOT NULL  ,
	finSession    DATE  NOT NULL  ,
	idFormateur   NUMBER(10,0)  NOT NULL  ,
	idFormation   NUMBER(10,0)  NOT NULL  ,
  CONSTRAINT Sessions_Pk PRIMARY KEY (idSession)
);

------------------------------------------------------------
-- Table: Formation
------------------------------------------------------------
CREATE TABLE Formation(
	idFormation     NUMBER(10,0)  NOT NULL  ,
	nomFormation    VARCHAR2 (25) NOT NULL UNIQUE,
	durerFormation  NUMBER(10,0)  NOT NULL  ,
	CONSTRAINT Formation_Pk PRIMARY KEY (idFormation)
);

------------------------------------------------------------
-- Table: Module
------------------------------------------------------------
CREATE TABLE Module(
	idModule     NUMBER(10,0)  NOT NULL  ,
	nomModule    VARCHAR2 (25) NOT NULL  ,
	idFormation  NUMBER(10,0)  NOT NULL  ,
	CONSTRAINT Module_Pk PRIMARY KEY (idModule)
);

------------------------------------------------------------
-- Table: Stagiaire
------------------------------------------------------------
CREATE TABLE Stagiaire(
	idStagiaire          NUMBER(10,0)  NOT NULL  ,
	nomStagiaire         VARCHAR2 (25) NOT NULL  ,
	prenomStagiaire      VARCHAR2 (25) NOT NULL  ,
	sexeStagiaire        CHAR (1)  NOT NULL  ,
  emailStagiaire        VARCHAR2 (25) NOT NULL  ,
	telStagiaire         VARCHAR2 (10) NOT NULL  ,
	nbCCP                NUMBER(10,0)   ,
	certification        NUMBER(10,0)  ,
	abandon              NUMBER (1) ,
	motifAbandon         VARCHAR2 (25)  ,
	villeStagiaire       VARCHAR2 (25) NOT NULL  ,
	codePostalStagiaire  VARCHAR2(5)  NOT NULL  ,
	nomRueStagiaire      VARCHAR2 (25) NOT NULL  ,
	numRueStagiaire      VARCHAR2(3)  NOT NULL  ,
	idSession            NUMBER(10,0)  ,
	CONSTRAINT Stagiaire_Pk PRIMARY KEY (idStagiaire) ,
	CONSTRAINT CHK_BOOLEAN_abandon CHECK (abandon IN (0,1))
);

ALTER TABLE Sessions ADD FOREIGN KEY (idFormateur) REFERENCES Formateur(idFormateur);
ALTER TABLE Sessions ADD FOREIGN KEY (idFormation) REFERENCES Formation(idFormation);
ALTER TABLE Module ADD FOREIGN KEY (idFormation) REFERENCES Formation(idFormation);
ALTER TABLE Stagiaire ADD FOREIGN KEY (idSession) REFERENCES Sessions(idSession);


--------------------------------------------------------------------------------
-- TRIGGER
--------------------------------------------------------------------------------

/*
 * Trigger INSERT et DELETE de la table formateur
 * @author Phil'Dy Belcou
 */
create or replace trigger triggerFormateur
before insert or delete on formateur
for each row
declare
  vNbValue int;
begin
  if (inserting) then
    :new.nomformateur := trim(initcap(:new.nomformateur));
    :new.prenomformateur := trim(initcap(:new.prenomformateur));
    :new.emailformateur := trim(lower(:new.emailformateur));
    
    -- V�rifie si le nom du formateur est dans un format correct
    if (not regexp_like(:new.nomformateur, '^[a-z]+$', 'i')) then
      raise_application_error(-20106, 'Veuillez saisir un nom de formateur correct');
    end if;
    
    -- V�rifie si le pr�nom du formateur est dans un format correct
    if (not regexp_like(:new.prenomformateur, '^[a-z]+$', 'i')) then
      raise_application_error(-20107, 'Veuillez saisir un pr�nom de formateur correct');
    end if;
    
    -- V�rifie si un formateur porte d�j� le couple nom/pr�nom
    select count(*) into vNbValue
      from formateur
        where nomformateur = :new.nomformateur and prenomformateur = :new.prenomformateur;
    if (vNbValue = 1) then
      raise_application_error(-20100, 'Ce formateur est d�j� enregistrer');
    end if;
    
    -- V�rifie si l'email est d�j� utilis� par un formateur
    select count(*) into vNbValue
      from formateur
        where emailformateur = :new.emailformateur;
    if (vNbValue = 1) then
      raise_application_error(-20101, 'Cette email est d�j� utilis�e par un formateur');
    end if;
    
    -- V�rifie si l'email a un format correct    
    if (not regexp_like (:new.emailformateur, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
      raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
    end if;
    
    -- V�rifie si le num�ro de t�l�phone est d�j� utilis� par un formateur
    select count(*) into vNbValue
      from formateur
        where telformateur = :new.telformateur;
    if (vNbValue = 1) then
      raise_application_error(-20103, 'Ce num�ro de t�l�phone est d�j� utilis� par un formateur');
    end if;
    
    -- V�rifie si le num�ro de t�l�phone a un format correct  
    if (length(:new.telformateur) != 10 or not regexp_like(:new.telformateur, '^[0-9]+$')) then
      raise_application_error(-20104, 'Le format du num�ro de t�l�phone est incorrect');
    end if;
    
    -- D�fini l'identifiant du formateur    
    select count(*) + 1 into vNbValue
      from formateur;
    :new.idformateur := vNbValue;
    
    -- D�fini le login du formateur
    :new.loginformateur := lower(substr(:new.prenomformateur, 0, 1)) || '.' || lower(:new.nomformateur);
    
    select count(*) into vNbValue -- Si un formateur a d�j� ce login, on ajoute un nombre au login
      from formateur
        where loginformateur = :new.loginformateur;
    if (vNbValue > 0) then
      :new.loginformateur := :new.loginformateur || vNbValue;
    end if;
    
    :new.mdpformateur := 'secu';
  elsif (deleting) then
    -- V�rifie si le formateur n'est affili� � aucune session
    select count(*) into vNbValue
      from sessions 
        where idformateur = :old.idformateur;
    if (vNbValue > 0) then
      raise_application_error(-20105, 'Le formateur est affili� � une session, il ne peut pas �tre supprim�');
    end if;
  end if;
end triggerFormateur;
/

--------------------------------------------------------------------------------
-- PROCEDURE
--------------------------------------------------------------------------------

/*
 * Procedure qui contr�le la connexion du formateur
 * @author : Phil'Dy Belcou
 */
create or replace procedure connection(pLogin varchar2, pMdp varchar2, 
                                      pNom out varchar2, pDroit out varchar2, pSexe out varchar2) as
  nbProfil int := 0;
  vEstAdmin formateur.estAdmin%type;
  vNom formateur.nomformateur%type;
  vPrenom formateur.prenomformateur%type;
begin
  -- V�rifie si le profil existe
  select count(*) into nbProfil
    from formateur
        where loginformateur = pLogin and mdpformateur = pMdp;
  
  if (nbProfil = 0) then -- S'il n'existe pas, la proc�dure renvoi une erreur
    raise_application_error(-20500, 'Profil inexistant');
  end if;
  
  select estadmin, nomformateur, prenomformateur 
    into vEstAdmin, vNom, vPrenom
      from formateur
        where loginformateur = pLogin;
  
  if (vEstAdmin = 1) then
    pDroit := 'Administrateur';
  else
    pDroit := 'Formateur';
  end if;
  
  pNom := vNom || ' ' || vPrenom;
  pSexe  := 'H';
end connection;
/

/*
 * Proc�dure qui modifie un formateur
 * @author Phil'Dy Belcou
 */
create or replace procedure modifierFormateur(nom varchar2, prenom varchar2, sexe char, email varchar2, tel varchar2, isadmin int, pId int) is
  vNbValue int;
begin
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  select count(*) into vNbValue
    from formateur
      where telformateur = tel and idformateur != pId;
  if (vNbValue = 1) then
    raise_application_error(-20103, 'Ce num�ro de t�l�phone est d�j� utilis� par un formateur');
  end if;
  
  if (length(tel) != 10) then
    raise_application_error(-20104, 'Le format du num�ro de t�l�phone est incorrect');
  end if;
  
  select count(*) into vNbValue
    from formateur
      where emailformateur = email and idformateur != pId;
  if (vNbValue = 1) then
    raise_application_error(-20101, 'Cette email est d�j� utilis�e par un formateur');
  end if;
  
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  update formateur 
    set nomformateur = nom,
        prenomformateur = prenom,
        sexeformateur = sexe,
        emailformateur = email,
        telformateur = tel,
        estadmin = isadmin
    where idformateur = pId;
end modifierFormateur;
/

/*
 * Proc�dure qui r�cup�re un formateur au � format profil �
 * @author : Phil'Dy Belcou
 */
create or replace procedure getProfil(pLogin varchar2, curProfil out sys_refcursor) is
begin
  open curProfil for
    select loginformateur, 
          nomformateur || ' ' || prenomformateur as "nom", 
          decode(estAdmin, 1, 'Administrateur', 'Formateur') as "droit", 
          sexeformateur as "sexe"
      from formateur
        where loginformateur = pLogin;
end getProfil;
/

--------------------------------------------------------------------------------
-- JEUX D'ESSAI
--------------------------------------------------------------------------------

/*
 * Jeux d'essais de la table formateur
 * @author Phil'Dy Belcou
 */
delete from formateur;

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Bin', 'Damien', 'H', 'bin@hotmail.fr', '0605040708', 1);

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Biglon', 'Josiane', 'F', 'biglon@hotmail.fr', '0606060606', 0);

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Favier', 'Sophie', 'F', 'favier@hotmail.fr', '0808080808', 0);
  
insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Delaroute', 'Luc', 'H', 'delaroute@hotmail.fr', '0909090909', 0);

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Rozana', 'Christine', 'F', 'rozana@hotmail.fr', '0505050505', 1);
commit;




-------------------------------------------------------------------------------
--------------------------------- FORMATION -----------------------------------
-------------------------------------------------------------------------------
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getListFormation(curFormation OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curFormation FOR
    SELECT IDFORMATION AS "id",
           NOMFORMATION AS "nom",
           DURERFORMATION AS "duree"
      FROM FORMATION
    ORDER BY IDFORMATION;
END getListFormation;
/
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getFormation(nom IN VARCHAR2, curFormation OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curFormation FOR
     SELECT IDFORMATION AS "id",
            NOMFORMATION AS "nom",
            DURERFORMATION AS "duree"
      FROM FORMATION
        WHERE NOMFORMATION = nom;
END getFormation;
/

/*
Autheur : Thomas Lecostey
Controle si :
              - nom existe deja
              - dur�e formation < 140 heures
              - Formation affili� a au moins une session lors de la suppression
*/
-------------------------------------------------------------------------------
------------------------------ TRIGGER FORMATION ------------------------------
-------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ctrlFormation
BEFORE INSERT OR DELETE ON FORMATION 
FOR EACH ROW
DECLARE 
  idFormation FORMATION.IDFORMATION%TYPE;
  nomFormation FORMATION.NOMFORMATION%TYPE;
  idFormationSessions NUMBER;
  idFormationModule NUMBER;
BEGIN
  IF(INSERTING) THEN
    SELECT MAX(IDFORMATION) INTO idFormation
      FROM FORMATION;
      
    SELECT COUNT(NOMFORMATION) INTO nomFormation
      FROM FORMATION
       WHERE UPPER(NOMFORMATION) = UPPER(:NEW.NOMFORMATION);
        
    IF(idFormation IS NULL) THEN
      :NEW.IDFORMATION := 1;
    ELSE
      :NEW.IDFORMATION := idFormation + 1;
    END IF;
    
    IF(nomFormation = 1) THEN
      RAISE_APPLICATION_ERROR(-20007, 'La formation ' || :NEW.NOMFORMATION || ' existe d�j�.');
    ELSIF(:NEW.DURERFORMATION < 140) THEN
      RAISE_APPLICATION_ERROR(-20016, 'La dur�e de la formation ne peux pas �tre en dessous de 140 heures (Environ 1 mois)'); 
    END IF; 
  ELSIF(DELETING) THEN
    SELECT COUNT(IDFORMATION) INTO idFormationSessions
      FROM SESSIONS
        WHERE IDFORMATION = :OLD.IDFORMATION;
    
    SELECT COUNT(IDFORMATION) INTO idFormationModule
      FROM MODULE
        WHERE IDFORMATION = :OLD.IDFORMATION;
    
    IF(idFormationSessions >= 1) THEN
       RAISE_APPLICATION_ERROR(-20902, 'La formation ' || :OLD.NOMFORMATION ||' est affili�e' || 
          ' � au moins une session, impossible de la supprim�e !');  
    ELSIF(idFormationModule >= 1) THEN    
       RAISE_APPLICATION_ERROR(-20905, 'La formation ' || :OLD.NOMFORMATION ||' est affili�e' || 
          ' � au moins un module, impossible de la supprim�e !');
    END IF;
  END IF;
END;
/

--------------- JEUX D'ESSAIS ---------------------
---------------------------------------------------
DELETE FROM FORMATION;

INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('T2AI', 1295);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('D�veloppeur Logiciel', 1190);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('Agent de propret�', 525);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('Administrateur R�seau', 1400);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('BTP', 920);
  
COMMIT;
/

-------------------------------------------------------------------------------
----------------------------------- SESSION -----------------------------------
-------------------------------------------------------------------------------

/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getListSession(curSession OUT SYS_REFCURSOR)
AS
BEGIN
   OPEN curSession FOR
    SELECT S.IDSESSION AS "idSession",
           NOMSESSION AS "nom",
           TO_CHAR(DEBUTSESSION, 'DD/MM/YYYY') AS "dateDebut",
           TO_CHAR(FINSESSION, 'DD/MM/YYYY') AS "dateFin",
           PRENOMFORMATEUR AS "formateur",
           NOMFORMATION AS "formation"
      FROM SESSIONS S
        INNER JOIN FORMATEUR F
          ON S.IDFORMATEUR = F.IDFORMATEUR
        INNER JOIN FORMATION FMT
          ON S.IDFORMATION = FMT.IDFORMATION
      ORDER BY S.IDSESSION;
END getListSession;
/

/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getSession(nom IN VARCHAR2, curSession OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curSession FOR
    SELECT S.IDSESSION AS "idSession",
           NOMSESSION AS "nom",
           TO_CHAR(DEBUTSESSION, 'DD/MM/YYYY') AS "dateDebut",
           TO_CHAR(FINSESSION, 'DD/MM/YYYY') AS "dateFin",
           PRENOMFORMATEUR AS "formateur",
           NOMFORMATION AS "formation"
      FROM SESSIONS S
        INNER JOIN FORMATEUR F
          ON S.IDFORMATEUR = F.IDFORMATEUR
        INNER JOIN FORMATION FMT
          ON S.IDFORMATION = FMT.IDFORMATION
    WHERE NOMSESSION = nom;
END getSession;
/

/*
Autheur : Thomas Lecostey
Controle si :
              - nom existe deja
              - la formation existe
              - le formateur existe
              - le formateur ne peux pas suivre 2 sessions en m�me temps
              - la date de d�but est < � la date actuelle
              - la date de fin est < � la date de d�but
*/
-------------------------------------------------------------------------------
--------------------------- TRIGGER SESSIONS ----------------------------------
-------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ctrlSessions
BEFORE INSERT OR DELETE ON SESSIONS 
FOR EACH ROW
DECLARE 
  idSession SESSIONS.IDSESSION%TYPE;
  nomSession SESSIONS.NOMSESSION%TYPE;
  dateDebut SESSIONS.DEBUTSESSION%TYPE;
  dateFinSession SESSIONS.FINSESSION%TYPE;
  idFormation FORMATION.IDFORMATION%TYPE;
  idFormateur FORMATEUR.IDFORMATEUR%TYPE;
  idSessionStagiaire NUMBER;

BEGIN
  IF(INSERTING) THEN
    SELECT MAX(IDSESSION) INTO idSession
      FROM SESSIONS;
        
    SELECT COUNT(NOMSESSION) INTO nomSession 
      FROM SESSIONS
        WHERE UPPER(nomSession) = UPPER(:NEW.NOMSESSION);
        
    SELECT COUNT(IDFORMATION) INTO idFormation
      FROM FORMATION
        WHERE idFormation = :NEW.IDFORMATION;
        
    SELECT COUNT(IDFORMATEUR) INTO idFormateur
      FROM FORMATEUR
        WHERE idFormateur = :NEW.IDFORMATEUR;
        
    SELECT MAX(FINSESSION) INTO dateFinSession
      FROM SESSIONS
        WHERE IDFORMATEUR = :NEW.IDFORMATEUR;
        
    IF(idSession IS NULL) THEN
      :NEW.IDSESSION := 1;
    ELSE
      :NEW.IDSESSION := idSession + 1;
    END IF;   
    
    IF(nomSession = 1) THEN
       RAISE_APPLICATION_ERROR(-20010, 'La session ' || :NEW.NOMSESSION 
          || ' existe d�j�.');
    ELSIF (idFormation = 0) THEN
       RAISE_APPLICATION_ERROR(-20011, 'La formation ayant l''identifiant '
          || :NEW.IDFORMATION || ' n''existe pas.');
    ELSIF (idFormateur = 0) THEN
       RAISE_APPLICATION_ERROR(-20012, 'Le formateur ayant l''identifiant ' 
          || :NEW.IDFORMATEUR || ' n''existe pas.');
    ELSIF (:NEW.DEBUTSESSION < dateFinSession) THEN
        RAISE_APPLICATION_ERROR(-20017, 'Le formateur ayant l''identifiant ' 
          || :NEW.IDFORMATEUR || ' ne peux pas suivre 2 sessions en m�me temps !');
    ELSIF (:NEW.DEBUTSESSION < SYSDATE -1) THEN
       RAISE_APPLICATION_ERROR(-20013, 'La date ' 
          || TO_CHAR(:NEW.DEBUTSESSION,'DD/MM/YY') 
          || ' est ant�rieure � la date '|| TO_CHAR(SYSDATE, 'DD/MM/YY') || ' !');
    ELSIF (:NEW.FINSESSION < :NEW.DEBUTSESSION) THEN
       RAISE_APPLICATION_ERROR(-20018, 'La date de fin de session ne peux pas �tre ant�rieure '
       || '� celle de d�but !');
    END IF;
  ELSIF(DELETING) THEN
    SELECT COUNT(IDSESSION) INTO idSessionStagiaire
      FROM STAGIAIRE
        WHERE IDSESSION = :OLD.IDSESSION; 
    IF(idSessionStagiaire >= 1) THEN
       RAISE_APPLICATION_ERROR(-20903, 'Au moins un stagiaire fais parti de cette session, ' 
        || 'impossible de la supprimer !');
    END IF;
  END IF;
END;
/

----------------------------------------------
------------- JEUX D'ESSAIS ------------------
DELETE FROM SESSIONS;

INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('TM1', '28/04/2020', '28/12/2020', 2, 1);

INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('DL24', '28/02/2019', '14/11/2019', 1, 2);
    
INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('AP1', '29/03/2019', '29/06/2019', 5, 3);
    
INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('AR5', '06/01/2018', '06/08/2018', 3, 4);
    
INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('BTP6', '06/01/2018', '13/04/2018', 4, 5);
COMMIT;
/

-------------------------------------------------------------------------------
---------------------------------- MODULE -------------------------------------
-------------------------------------------------------------------------------

/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getListModule(curModule OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curModule FOR
    SELECT IDMODULE AS "idModule",
           NOMMODULE AS "nom",
           NOMFORMATION AS "formation"
      FROM MODULE M
    INNER JOIN FORMATION F
      ON M.IDFORMATION = F.IDFORMATION
    ORDER BY IDMODULE; 
END getListModule;
/
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getModule(nom IN VARCHAR2, curModule OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curModule FOR
    SELECT IDMODULE AS "idModule",
           NOMMODULE AS "nom",
          NOMFORMATION AS "formation"
      FROM MODULE M 
       INNER JOIN FORMATION F
         ON M.IDFORMATION = F.IDFORMATION
    WHERE NOMMODULE = nom; 
END getModule;
/

/*
Autheur : Thomas Lecostey
Controle si :
              - nom existe deja
              - la formation existe
*/
-------------------------------------------------------------------------------
--------------------------------- TRIGGER MODULE ------------------------------
-------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ctrlModule
BEFORE INSERT OR DELETE ON MODULE 
FOR EACH ROW
DECLARE 
  idModule MODULE.IDMODULE%TYPE;
  nomModule MODULE.NOMMODULE%TYPE;
  idFormation FORMATION.IDFORMATION%TYPE;
  idModuleEval NUMBER;
BEGIN
IF(INSERTING) THEN
  SELECT MAX(IDMODULE) INTO idModule
   FROM MODULE;
    
  SELECT COUNT(NOMMODULE) INTO nomModule
    FROM MODULE
     WHERE TRIM(UPPER(NOMMODULE)) = TRIM(UPPER(:NEW.NOMMODULE)); 
    
  SELECT COUNT(IDFORMATION) INTO idFormation
  FROM FORMATION
    WHERE IDFORMATION = :NEW.IDFORMATION;
    
  IF(idModule IS NULL) THEN
    :NEW.IDMODULE := 1;
  ELSE
    :NEW.IDMODULE := idModule + 1;
  END IF;
  
  IF(nomModule = 1) THEN
    RAISE_APPLICATION_ERROR(-20008, 'Le module ' || UPPER(:NEW.NOMMODULE) || ' existe d�j�.');
  ELSIF (idFormation = 0) THEN
    RAISE_APPLICATION_ERROR(-20009, 'La formation avec l''ID ' || :NEW.IDFORMATION || ' n''existe pas.');
  END IF;
  
  /*ELSIF(DELETING) THEN
    SELECT COUNT(IDMODULE) INTO idModuleEval
     FROM EVALUATION
      WHERE IDMODULE = :OLD.IDMODULE;
      
    IF(idModuleEval >= 1) THEN
      RAISE_APPLICATION_ERROR(-20904, 'Le module ' || :OLD.NOMMODULE ||' est affili�' || 
          ' � au moins une �valuation, impossible de le supprim� !');
    END IF;*/
  END IF;
END;
/

--------------- JEUX D'ESSAIS ---------------------
---------------------------------------------------
DELETE FROM MODULE;

INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('D�monter un pc',1);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Nettoyer un pc',1);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Java',2);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Web',2);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Laver le sol',3);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Connexions TCP',4);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Construire un mur',5);
  
COMMIT;


--------------------------------------------------------------------------------
--------------------------------- STAGIAIRE ------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- TRIGGER
--------------------------------------------------------------------------------

/*
 * Trigger INSERT et DELETE de la table stagiaire
 * @author Phil'Dy Belcou
 */
 create or replace trigger triggerStagiaire
before insert or delete on stagiaire
for each row
declare
  vNbValue int;
begin
  if (inserting) then
    :new.nomstagiaire := trim(initcap(:new.nomstagiaire));
    :new.prenomstagiaire := trim(initcap(:new.prenomstagiaire));
    :new.emailstagiaire := trim(lower(:new.emailstagiaire));
    
    -- V�rifie si le nom du stagiaire est dans un format correct
    if (not regexp_like(:new.nomstagiaire, '^[a-z]+$', 'i')) then
      raise_application_error(-20150, 'Veuillez saisir un nom de stagiaire correct');
    elsif (not regexp_like(:new.prenomstagiaire, '^[a-z]+$', 'i')) then -- V�rifie si le pr�nom du stagiaire est dans un format correct
      raise_application_error(-20151, 'Veuillez saisir un pr�nom de stagiaire correct');
    end if;
    
    -- V�rifie si un stagiaire porte d�j� le couple nom/pr�nom
    select count(*) into vNbValue
      from stagiaire
        where nomstagiaire = :new.nomstagiaire and prenomstagiaire = :new.prenomstagiaire;
    if (vNbValue = 1) then
      raise_application_error(-20152, 'Ce stagiaire est d�j� enregistrer');
    end if;
    
    -- V�rifie si l'email est correct et si elle est d�j� utilis� par un stagiaire
    select count(*) into vNbValue
      from stagiaire
        where emailstagiaire = :new.emailstagiaire;
    if (vNbValue = 1) then
      raise_application_error(-20153, 'Cette email est d�j� utilis�e par un stagiaire');
    elsif (not regexp_like (:new.emailstagiaire, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
      raise_application_error(-20154, 'Le format de l''adresse email est incorrect');
    end if;
    
    -- V�rifie si le num�ro de t�l�phone est correct et s'il est d�j� utilis� par un stagiaire
    select count(*) into vNbValue
      from stagiaire
        where telstagiaire = :new.telstagiaire;
    if (vNbValue = 1) then
      raise_application_error(-20155, 'Ce num�ro de t�l�phone est d�j� utilis� par un stagiaire');
    elsif (length(:new.telstagiaire) != 10 or not regexp_like(:new.telstagiaire, '^[0-9]+$')) then
      raise_application_error(-20156, 'Le format du num�ro de t�l�phone est incorrect');
    end if;
    
    -- V�rifie l'adresse
    if (not regexp_like(:new.villestagiaire, '^[a-z]+$', 'i')) then
      raise_application_error(-20157, 'La ville du stagiaire a un format incorrect');
    elsif (length(:new.codepostalstagiaire) != 5 or not regexp_like(:new.codepostalstagiaire, '^[0-9]+$')) then
      raise_application_error(-20158, 'Le code postal n''a pas un bon format');
    elsif (not regexp_like(:new.nomruestagiaire, '^[a-z ]+$', 'i')) then
      raise_application_error(-20159, 'Le nom de rue du stagiaire a un format incorrect');
    elsif (length(:new.numruestagiaire) > 999 or not regexp_like(:new.numruestagiaire, '^[0-9]+$')) then
      raise_application_error(-20160, 'Le num�ro de rue du stagiaire a un format incorrect');
    end if;
    
    -- D�fini l'identifiant du stagiaire    
    select count(*) + 1 into vNbValue
      from stagiaire;
    :new.idstagiaire := vNbValue;
  elsif (deleting) then
    -- V�rifie si le stagiaire est en session
    select count(*) into vNbValue
      from sessions s 
        where s.idsession = :old.idsession
        and s.debutsession < to_char(sysdate, 'dd/mm/yyyy')
        and s.finsession > to_char(sysdate, 'dd/mm/yyyy');
    if (vNbValue > 0) then
      raise_application_error(-20105, 'Le stagiaire est en session, il ne peut pas �tre supprim�');
    end if;
  end if;
end triggerStagiaire;
/

--------------------------------------------------------------------------------
-- PROCEDURE
--------------------------------------------------------------------------------


/*
 * Proc�dure qui modifie un stagiaire
 * @author Phil'Dy Belcou
 */
create or replace procedure modifierStagiaire(nom varchar2, prenom varchar2, sexe char, email varchar2, 
                                              tel varchar2, ville varchar2, code varchar2, nomrue varchar2, numrue varchar2, pId int) is
  vNbValue int;
begin

  -- V�rifie si le nom du stagiaire est dans un format correct
  if (not regexp_like(nom, '^[a-z]+$', 'i')) then
    raise_application_error(-20150, 'Veuillez saisir un nom de stagiaire correct');
  elsif (not regexp_like(prenom, '^[a-z]+$', 'i')) then -- V�rifie si le pr�nom du stagiaire est dans un format correct
    raise_application_error(-20151, 'Veuillez saisir un pr�nom de stagiaire correct');
  end if;
  
  -- V�rifie si un stagiaire porte d�j� le couple nom/pr�nom
  select count(*) into vNbValue
    from stagiaire
      where nomstagiaire = nom and prenomstagiaire = prenom and idstagiaire != pId;
  if (vNbValue = 1) then
    raise_application_error(-20152, 'Ce stagiaire est d�j� enregistrer');
  end if;
  
  -- V�rifie si l'email est correct et si elle est d�j� utilis� par un stagiaire
  select count(*) into vNbValue
    from stagiaire
      where emailstagiaire = email and idstagiaire != pId;
  if (vNbValue = 1) then
    raise_application_error(-20153, 'Cette email est d�j� utilis�e par un stagiaire');
  elsif (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20154, 'Le format de l''adresse email est incorrect');
  end if;
  
  -- V�rifie si le num�ro de t�l�phone est correct et s'il est d�j� utilis� par un stagiaire
  select count(*) into vNbValue
    from stagiaire
      where telstagiaire = tel and idstagiaire != pId;
  if (vNbValue = 1) then
    raise_application_error(-20155, 'Ce num�ro de t�l�phone est d�j� utilis� par un stagiaire');
  elsif (length(tel) != 10 or not regexp_like(tel, '^[0-9]+$')) then
    raise_application_error(-20156, 'Le format du num�ro de t�l�phone est incorrect');
  end if;
  
  -- V�rifie l'adresse
  if (not regexp_like(ville, '^[a-z]+$', 'i')) then
    raise_application_error(-20157, 'La ville du stagiaire a un format incorrect');
  elsif (length(code) != 5 or not regexp_like(code, '^[0-9]+$')) then
    raise_application_error(-20158, 'Le code postal n''a pas un bon format');
  elsif (not regexp_like(nomrue, '^[a-z ]+$', 'i')) then
    raise_application_error(-20159, 'Le nom de rue du stagiaire a un format incorrect');
  elsif (length(numrue) > 3 or not regexp_like(numrue, '^[0-9]+$')) then
    raise_application_error(-20160, 'Le num�ro de rue du stagiaire a un format incorrect');
  end if;
  
  update stagiaire 
    set nomstagiaire = nom,
        prenomstagiaire = prenom,
        sexestagiaire = sexe,
        emailstagiaire = email,
        telstagiaire = tel,
        villestagiaire = ville,
        codepostalstagiaire = code,
        nomruestagiaire = nomrue,
        numruestagiaire = numrue
    where idstagiaire = pId;
end modifierStagiaire;
/


--------------------------------------------------------------------------------
-- JEUX D'ESSAI
--------------------------------------------------------------------------------

/*
 * Jeux d'essais de la table formateur
 * @author Phil'Dy Belcou
 */
delete from stagiaire;

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(1, 'Pavin', 'Marcelino', 'H', 'pavin@hotmail.fr', '0685347825', 'Toulouse', 31200, 'Rue de la brioche', 5);
  
insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(2, 'Gardin', 'Jos�phine', 'F', 'gardin@hotmail.fr', '0641278423', 'Caen', 14000, 'Rue du magicien', 1);

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(3, 'Snow', 'John', 'H', 'snow@hotmail.fr', '0635412792', 'Paris', 75001, 'Rue du pain confiture', 15);

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(4, 'Moss', 'Angela', 'F', 'moss@hotmail.fr', '0631478256', 'Rouen', 76000, 'Rue de la belle rue', 2);

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire, idsession) 
  values(5, 'Leslie', 'Rose', 'F', 'leslie@hotmail.fr', '0682924578', 'Paris', 75001, 'Rue du pain confiture', 15, 2);
delete from stagiaire where nomstagiaire = 'Leslie';
commit;