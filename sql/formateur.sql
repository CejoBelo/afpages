
--------------------------------------------------------------------------------
-- TRIGGER
--------------------------------------------------------------------------------

/*
 * Trigger INSERT et DELETE de la table formateur
 * @author Phil'Dy Belcou
 */
create or replace trigger triggerFormateur
before insert or delete on formateur
for each row
declare
  vNbValue int;
begin
  if (inserting) then
    :new.nomformateur := trim(initcap(:new.nomformateur));
    :new.prenomformateur := trim(initcap(:new.prenomformateur));
    :new.emailformateur := trim(lower(:new.emailformateur));
    
    -- V�rifie si le nom du formateur est dans un format correct
    if (not regexp_like(:new.nomformateur, '^[a-z]+$', 'i')) then
      raise_application_error(-20106, 'Veuillez saisir un nom de formateur correct');
    end if;
    
    -- V�rifie si le pr�nom du formateur est dans un format correct
    if (not regexp_like(:new.prenomformateur, '^[a-z]+$', 'i')) then
      raise_application_error(-20107, 'Veuillez saisir un pr�nom de formateur correct');
    end if;
    
    -- V�rifie si un formateur porte d�j� le couple nom/pr�nom
    select count(*) into vNbValue
      from formateur
        where nomformateur = :new.nomformateur and prenomformateur = :new.prenomformateur;
    if (vNbValue = 1) then
      raise_application_error(-20100, 'Ce formateur est d�j� enregistrer');
    end if;
    
    -- V�rifie si l'email est d�j� utilis� par un formateur
    select count(*) into vNbValue
      from formateur
        where emailformateur = :new.emailformateur;
    if (vNbValue = 1) then
      raise_application_error(-20101, 'Cette email est d�j� utilis�e par un formateur');
    end if;
    
    -- V�rifie si l'email a un format correct    
    if (not regexp_like (:new.emailformateur, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
      raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
    end if;
    
    -- V�rifie si le num�ro de t�l�phone est d�j� utilis� par un formateur
    select count(*) into vNbValue
      from formateur
        where telformateur = :new.telformateur;
    if (vNbValue = 1) then
      raise_application_error(-20103, 'Ce num�ro de t�l�phone est d�j� utilis� par un formateur');
    end if;
    
    -- V�rifie si le num�ro de t�l�phone a un format correct  
    if (length(:new.telformateur) != 10 or not regexp_like(:new.telformateur, '^[0-9]+$')) then
      raise_application_error(-20104, 'Le format du num�ro de t�l�phone est incorrect');
    end if;
    
    -- D�fini l'identifiant du formateur    
    select count(*) + 1 into vNbValue
      from formateur;
    :new.idformateur := vNbValue;
    
    -- D�fini le login du formateur
    :new.loginformateur := lower(substr(:new.prenomformateur, 0, 1)) || '.' || lower(:new.nomformateur);
    
    select count(*) into vNbValue -- Si un formateur a d�j� ce login, on ajoute un nombre au login
      from formateur
        where loginformateur = :new.loginformateur;
    if (vNbValue > 0) then
      :new.loginformateur := :new.loginformateur || vNbValue;
    end if;
    
    :new.mdpformateur := 'secu';
  elsif (deleting) then
    -- V�rifie si le formateur n'est affili� � aucune session
    select count(*) into vNbValue
      from sessions 
        where idformateur = :old.idformateur;
    if (vNbValue > 0) then
      raise_application_error(-20105, 'Le formateur est affili� � une session, il ne peut pas �tre supprim�');
    end if;
  end if;
end triggerFormateur;
/

--------------------------------------------------------------------------------
-- PROCEDURE
--------------------------------------------------------------------------------

/*
 * Procedure qui contr�le la connexion du formateur
 * @author : Phil'Dy Belcou
 */
create or replace procedure connection(pLogin varchar2, pMdp varchar2, 
                                      pNom out varchar2, pDroit out varchar2, pSexe out varchar2) as
  nbProfil int := 0;
  vEstAdmin formateur.estAdmin%type;
  vNom formateur.nomformateur%type;
  vPrenom formateur.prenomformateur%type;
begin
  -- V�rifie si le profil existe
  select count(*) into nbProfil
    from formateur
        where loginformateur = pLogin and mdpformateur = pMdp;
  
  if (nbProfil = 0) then -- S'il n'existe pas, la proc�dure renvoi une erreur
    raise_application_error(-20500, 'Profil inexistant');
  end if;
  
  select estadmin, nomformateur, prenomformateur 
    into vEstAdmin, vNom, vPrenom
      from formateur
        where loginformateur = pLogin;
  
  if (vEstAdmin = 1) then
    pDroit := 'Administrateur';
  else
    pDroit := 'Formateur';
  end if;
  
  pNom := vNom || ' ' || vPrenom;
  pSexe  := 'H';
end connection;
/

/*
 * Proc�dure qui modifie un formateur
 * @author Phil'Dy Belcou
 */
create or replace procedure modifierFormateur(nom varchar2, prenom varchar2, sexe char, email varchar2, tel varchar2, isadmin int, pId int) is
  vNbValue int;
begin
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  select count(*) into vNbValue
    from formateur
      where telformateur = tel and idformateur != pId;
  if (vNbValue = 1) then
    raise_application_error(-20103, 'Ce num�ro de t�l�phone est d�j� utilis� par un formateur');
  end if;
  
  if (length(tel) != 10) then
    raise_application_error(-20104, 'Le format du num�ro de t�l�phone est incorrect');
  end if;
  
  select count(*) into vNbValue
    from formateur
      where emailformateur = email and idformateur != pId;
  if (vNbValue = 1) then
    raise_application_error(-20101, 'Cette email est d�j� utilis�e par un formateur');
  end if;
  
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  update formateur 
    set nomformateur = nom,
        prenomformateur = prenom,
        sexeformateur = sexe,
        emailformateur = email,
        telformateur = tel,
        estadmin = isadmin
    where idformateur = pId;
end modifierFormateur;
/

/*
 * Proc�dure qui r�cup�re un formateur au � format profil �
 * @author : Phil'Dy Belcou
 */
create or replace procedure getProfil(pLogin varchar2, curProfil out sys_refcursor) is
begin
  open curProfil for
    select loginformateur, 
          nomformateur || ' ' || prenomformateur as "nom", 
          decode(estAdmin, 1, 'Administrateur', 'Formateur') as "droit", 
          sexeformateur as "sexe"
      from formateur
        where loginformateur = pLogin;
end getProfil;
/

--------------------------------------------------------------------------------
-- JEUX D'ESSAI
--------------------------------------------------------------------------------

/*
 * Jeux d'essais de la table formateur
 * @author Phil'Dy Belcou
 */
delete from formateur;

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Bin', 'Damien', 'H', 'bin@hotmail.fr', '0605040708', 1);

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Biglon', 'Josiane', 'F', 'biglon@hotmail.fr', '0606060606', 0);

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Favier', 'Sophie', 'F', 'favier@hotmail.fr', '0808080808', 0);
  
insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Delaroute', 'Luc', 'H', 'delaroute@hotmail.fr', '0909090909', 0);

insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Rozana', 'Christine', 'F', 'rozana@hotmail.fr', '0505050505', 1);
commit;

