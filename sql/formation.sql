-------------------------------------------------------------------------------
--------------------------------- FORMATION -----------------------------------
-------------------------------------------------------------------------------
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getListFormation(curFormation OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curFormation FOR
    SELECT IDFORMATION AS "id",
           NOMFORMATION AS "nom",
           DURERFORMATION AS "duree"
      FROM FORMATION
    ORDER BY IDFORMATION;
END getListFormation;
/
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getFormation(nom IN VARCHAR2, curFormation OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curFormation FOR
     SELECT IDFORMATION AS "id",
            NOMFORMATION AS "nom",
            DURERFORMATION AS "duree"
      FROM FORMATION
        WHERE NOMFORMATION = nom;
END getFormation;
/

/*
Autheur : Thomas Lecostey
Controle si :
              - nom existe deja
              - dur�e formation < 140 heures
              - Formation affili� a au moins une session lors de la suppression
*/
-------------------------------------------------------------------------------
------------------------------ TRIGGER FORMATION ------------------------------
-------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ctrlFormation
BEFORE INSERT OR DELETE ON FORMATION 
FOR EACH ROW
DECLARE 
  idFormation FORMATION.IDFORMATION%TYPE;
  nomFormation FORMATION.NOMFORMATION%TYPE;
  idFormationSessions NUMBER;
BEGIN
  IF(INSERTING) THEN
    SELECT MAX(IDFORMATION) INTO idFormation
      FROM FORMATION;
      
    SELECT COUNT(NOMFORMATION) INTO nomFormation
      FROM FORMATION
       WHERE UPPER(NOMFORMATION) = UPPER(:NEW.NOMFORMATION);
        
    IF(idFormation IS NULL) THEN
      :NEW.IDFORMATION := 1;
    ELSE
      :NEW.IDFORMATION := idFormation + 1;
    END IF;
    
    IF(nomFormation = 1) THEN
      RAISE_APPLICATION_ERROR(-20007, 'La formation ' || :NEW.NOMFORMATION || ' existe d�j�.');
    ELSIF(:NEW.DURERFORMATION < 140) THEN
      RAISE_APPLICATION_ERROR(-20016, 'La dur�e de la formation ne peux pas �tre en dessous de 140 heures (Environ 1 mois)'); 
    END IF; 
  ELSIF(DELETING) THEN
    SELECT COUNT(IDFORMATION) INTO idFormationSessions
      FROM SESSIONS
        WHERE IDFORMATION = :OLD.IDFORMATION;
    
    IF(idFormationSessions >= 1) THEN
       RAISE_APPLICATION_ERROR(-20902, 'La formation ' || :OLD.NOMFORMATION ||' est affili�e' || 
          ' � au moins une session, impossible de la supprim�e !');  
    END IF;
  END IF;
END;
/

--------------- JEUX D'ESSAIS ---------------------
---------------------------------------------------
DELETE FROM FORMATION;

INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('T2AI', 1295);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('D�veloppeur Logiciel', 1190);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('Agent de propret�', 525);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('Administrateur R�seau', 1400);
  
INSERT INTO FORMATION (NOMFORMATION, DURERFORMATION)
  VALUES('BTP', 920);
  
COMMIT;