SET SERVEROUTPUT ON

-------------- INSERTION FORMATION --------------
delete formation;
insert into formation values(1, 'Développeur logiciel', 8760);
-------------------------------------------------
-------------- INSERTION STAGIAIRE --------------
delete from stagiaire;
insert into stagiaire(nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire)
  values('Belcou', 'Phil''Dy', 'H', 'belcou@gmail.com', '0011223345', 'Caen', '14000', 'Rue de Rosel', '10');
-------------------------------------------------


-------------- INSERTION FORMATEUR --------------
delete from formateur;
insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Belcou', 'Phil''Dy', 'H', 'belcou@gmail.com', '0011223345', 1);
insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Dupont', 'Emma', 'F', 'dupont@gmail.com', '0011923345', 0);
insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) 
  values('Lecostey', 'Thomas', 'H', 'lecostey@gmail.com', '0000000000', 0);
-------------------------------------------------

--------------- INSERTION SESSION ---------------
delete from sessions;
--insert into sessions values(1, 'DL1', '30/11/2017', '26/11/2018', 1, 1);
-------------------------------------------------

commit;