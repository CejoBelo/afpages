-------------------------------------------------------------------------------
---------------------------------- MODULE -------------------------------------
-------------------------------------------------------------------------------

/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getListModule(curModule OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curModule FOR
    SELECT IDMODULE AS "idModule",
           NOMMODULE AS "nom",
           NOMFORMATION AS "formation"
      FROM MODULE M
    INNER JOIN FORMATION F
      ON M.IDFORMATION = F.IDFORMATION
    ORDER BY IDMODULE; 
END getListModule;
/
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getModule(nom IN VARCHAR2, curModule OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curModule FOR
    SELECT IDMODULE AS "idModule",
           NOMMODULE AS "nom",
          NOMFORMATION AS "formation"
      FROM MODULE M 
       INNER JOIN FORMATION F
         ON M.IDFORMATION = F.IDFORMATION
    WHERE NOMMODULE = nom; 
END getModule;
/

/*
Autheur : Thomas Lecostey
Controle si :
              - nom existe deja
              - la formation existe
*/
-------------------------------------------------------------------------------
--------------------------------- TRIGGER MODULE ------------------------------
-------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ctrlModule
BEFORE INSERT OR DELETE ON MODULE 
FOR EACH ROW
DECLARE 
  idModule MODULE.IDMODULE%TYPE;
  nomModule MODULE.NOMMODULE%TYPE;
  idFormation FORMATION.IDFORMATION%TYPE;
  idModuleEval NUMBER;
BEGIN
IF(INSERTING) THEN
  SELECT MAX(IDMODULE) INTO idModule
   FROM MODULE;
    
  SELECT COUNT(NOMMODULE) INTO nomModule
    FROM MODULE
     WHERE TRIM(UPPER(NOMMODULE)) = TRIM(UPPER(:NEW.NOMMODULE)); 
    
  SELECT COUNT(IDFORMATION) INTO idFormation
  FROM FORMATION
    WHERE IDFORMATION = :NEW.IDFORMATION;
    
  IF(idModule IS NULL) THEN
    :NEW.IDMODULE := 1;
  ELSE
    :NEW.IDMODULE := idModule + 1;
  END IF;
  
  IF(nomModule = 1) THEN
    RAISE_APPLICATION_ERROR(-20008, 'Le module ' || UPPER(:NEW.NOMMODULE) || ' existe d�j�.');
  ELSIF (idFormation = 0) THEN
    RAISE_APPLICATION_ERROR(-20009, 'La formation avec l''ID ' || :NEW.IDFORMATION || ' n''existe pas.');
  END IF;
  
  ELSIF(DELETING) THEN
    SELECT COUNT(IDMODULE) INTO idModuleEval
     FROM EVALUATION
      WHERE IDMODULE = :OLD.IDMODULE;
      
    IF(idModuleEval >= 1) THEN
      RAISE_APPLICATION_ERROR(-20904, 'Le module ' || :OLD.NOMMODULE ||' est affili�' || 
          ' � au moins une �valuation, impossible de le supprim� !');
    END IF;
  END IF;
END;
/

--------------- JEUX D'ESSAIS ---------------------
---------------------------------------------------
DELETE FROM MODULE;

INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('D�monter un pc',1);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Nettoyer un pc',1);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Java',2);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Web',2);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Laver le sol',3);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Connexions TCP',4);
  
INSERT INTO MODULE(NOMMODULE, IDFORMATION)
  VALUES('Construire un mur',5);
  
COMMIT;