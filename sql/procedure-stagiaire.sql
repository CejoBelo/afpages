
create or replace procedure checkStagiaire (nom varchar2, prenom varchar2, email varchar2, tel varchar2) is
  vNbValue int;
begin
  select count(*) into vNbValue
    from stagiaire
      where nomstagiaire = nom and prenomstagiaire = prenom;
  if (vNbValue = 1) then
    raise_application_error(-20100, 'Ce stagiaire est d�j� enregistrer');
  end if;
  
  select count(*) into vNbValue
    from stagiaire
      where emailstagiaire = email;
  if (vNbValue = 1) then
    raise_application_error(-20101, 'Cette email est d�j� utilis�e par un stagiaire');
  end if;
  
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  select count(*) into vNbValue
    from stagiaire
      where telstagiaire = tel;
  if (vNbValue = 1) then
    raise_application_error(-20103, 'Ce num�ro de t�l�phone est d�j� utilis� par un stagiaire');
  end if;
  
  if (length(tel) != 10) then
    raise_application_error(-20104, 'Le format du num�ro de t�l�phone est incorrect');
  end if;
end checkStagiaire;
/
/*
create or replace procedure modifierFormateur(nom varchar2, prenom varchar2, sexe char, email varchar2, tel varchar2, estAdmin int, pId int) is
  vNbValue int;
begin
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  select count(*) into vNbValue
    from stagiaire
      where telformateur = tel and idformateur != pId;
  if (vNbValue = 1) then
    raise_application_error(-20103, 'Ce num�ro de t�l�phone est d�j� utilis� par un formateur');
  end if;
  
  if (length(tel) != 10) then
    raise_application_error(-20104, 'Le format du num�ro de t�l�phone est incorrect');
  end if;
  
  select count(*) into vNbValue
    from stagiaire
      where emailformateur = email and idformateur != pId;
  if (vNbValue = 1) then
    raise_application_error(-20101, 'Cette email est d�j� utilis�e par un formateur');
  end if;
  
  if (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20102, 'Le format de l''adresse email est incorrect');
  end if;
  
  update stagiaire 
    set nomformateur = nom,
        prenomformateur = prenom,
        sexeformateur = sexe,
        emailformateur = email,
        telformateur = tel
    where idformateur = pId;
end modifierFormateur;
/*/