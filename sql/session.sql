-------------------------------------------------------------------------------
----------------------------------- SESSION -----------------------------------
-------------------------------------------------------------------------------

/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getListSession(curSession OUT SYS_REFCURSOR)
AS
BEGIN
   OPEN curSession FOR
    SELECT S.IDSESSION AS "idSession",
           NOMSESSION AS "nom",
           TO_CHAR(DEBUTSESSION, 'DD/MM/YYYY') AS "dateDebut",
           TO_CHAR(FINSESSION, 'DD/MM/YYYY') AS "dateFin",
           PRENOMFORMATEUR AS "formateur",
           NOMFORMATION AS "formation"
      FROM SESSIONS S
        INNER JOIN FORMATEUR F
          ON S.IDFORMATEUR = F.IDFORMATEUR
        INNER JOIN FORMATION FMT
          ON S.IDFORMATION = FMT.IDFORMATION
      ORDER BY S.IDSESSION;
END getListSession;
/
/*
* Author : Thomas Lecostey
*/
create or replace PROCEDURE getSession(nom IN VARCHAR2, curSession OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN curSession FOR
    SELECT S.IDSESSION AS "idSession",
           NOMSESSION AS "nom",
           TO_CHAR(DEBUTSESSION, 'DD/MM/YYYY') AS "dateDebut",
           TO_CHAR(FINSESSION, 'DD/MM/YYYY') AS "dateFin",
           PRENOMFORMATEUR AS "formateur",
           NOMFORMATION AS "formation"
      FROM SESSIONS S
        INNER JOIN FORMATEUR F
          ON S.IDFORMATEUR = F.IDFORMATEUR
        INNER JOIN FORMATION FMT
          ON S.IDFORMATION = FMT.IDFORMATION
    WHERE NOMSESSION = nom;
END getSession;

/*
Autheur : Thomas Lecostey
Controle si :
              - nom existe deja
              - la formation existe
              - le formateur existe
              - le formateur ne peux pas suivre 2 sessions en m�me temps
              - la date de d�but est < � la date actuelle
              - la date de fin est < � la date de d�but
*/
-------------------------------------------------------------------------------
--------------------------- TRIGGER SESSIONS ----------------------------------
-------------------------------------------------------------------------------
CREATE OR REPLACE TRIGGER ctrlSessions
BEFORE INSERT OR DELETE ON SESSIONS 
FOR EACH ROW
DECLARE 
  idSession SESSIONS.IDSESSION%TYPE;
  nomSession SESSIONS.NOMSESSION%TYPE;
  dateDebut SESSIONS.DEBUTSESSION%TYPE;
  dateFinSession SESSIONS.FINSESSION%TYPE;
  idFormation FORMATION.IDFORMATION%TYPE;
  idFormateur FORMATEUR.IDFORMATEUR%TYPE;
  idSessionStagiaire NUMBER;

BEGIN
  IF(INSERTING) THEN
    SELECT MAX(IDSESSION) INTO idSession
      FROM SESSIONS;
        
    SELECT COUNT(NOMSESSION) INTO nomSession 
      FROM SESSIONS
        WHERE UPPER(nomSession) = UPPER(:NEW.NOMSESSION);
        
    SELECT COUNT(IDFORMATION) INTO idFormation
      FROM FORMATION
        WHERE idFormation = :NEW.IDFORMATION;
        
    SELECT COUNT(IDFORMATEUR) INTO idFormateur
      FROM FORMATEUR
        WHERE idFormateur = :NEW.IDFORMATEUR;
        
    SELECT MAX(FINSESSION) INTO dateFinSession
      FROM SESSIONS
        WHERE IDFORMATEUR = :NEW.IDFORMATEUR;
        
    IF(idSession IS NULL) THEN
      :NEW.IDSESSION := 1;
    ELSE
      :NEW.IDSESSION := idSession + 1;
    END IF;   
    
    IF(nomSession = 1) THEN
       RAISE_APPLICATION_ERROR(-20010, 'La session ' || :NEW.NOMSESSION 
          || ' existe d�j�.');
    ELSIF (idFormation = 0) THEN
       RAISE_APPLICATION_ERROR(-20011, 'La formation ayant l''identifiant '
          || :NEW.IDFORMATION || ' n''existe pas.');
    ELSIF (idFormateur = 0) THEN
       RAISE_APPLICATION_ERROR(-20012, 'Le formateur ayant l''identifiant ' 
          || :NEW.IDFORMATEUR || ' n''existe pas.');
    ELSIF (:NEW.DEBUTSESSION < dateFinSession) THEN
        RAISE_APPLICATION_ERROR(-20017, 'Le formateur ayant l''identifiant ' 
          || :NEW.IDFORMATEUR || ' ne peux pas suivre 2 sessions en m�me temps !');
    ELSIF (:NEW.DEBUTSESSION < SYSDATE -1) THEN
       RAISE_APPLICATION_ERROR(-20013, 'La date ' 
          || TO_CHAR(:NEW.DEBUTSESSION,'DD/MM/YY') 
          || ' est ant�rieure � la date '|| TO_CHAR(SYSDATE, 'DD/MM/YY') || ' !');
    ELSIF (:NEW.FINSESSION < :NEW.DEBUTSESSION) THEN
       RAISE_APPLICATION_ERROR(-20018, 'La date de fin de session ne peux pas �tre ant�rieure '
       || '� celle de d�but !');
    END IF;
  ELSIF(DELETING) THEN
    SELECT COUNT(IDSESSION) INTO idSessionStagiaire
      FROM STAGIAIRE
        WHERE IDSESSION = :OLD.IDSESSION; 
    IF(idSessionStagiaire >= 1) THEN
       RAISE_APPLICATION_ERROR(-20903, 'Au moins un stagiaire fais parti de cette session, ' 
        || 'impossible de la supprimer !');
    END IF;
  END IF;
END;
/

----------------------------------------------
------------- JEUX D'ESSAIS ------------------
DELETE FROM SESSIONS;

INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('','','',1,1);

INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('DL24','30/11/17','10/06/18',1,2);
    
INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('','','',1,2);
    
INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('','','',1,2);
    
INSERT INTO SESSIONS (NOMSESSION,DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION)
    VALUES('','','',1,2);
    
COMMIT;