
--------------------------------------------------------------------------------
-- TRIGGER
--------------------------------------------------------------------------------

/*
 * Trigger INSERT et DELETE de la table stagiaire
 * @author Phil'Dy Belcou
 */
 create or replace trigger triggerStagiaire
before insert or delete on stagiaire
for each row
declare
  vNbValue int;
begin
  if (inserting) then
    :new.nomstagiaire := trim(initcap(:new.nomstagiaire));
    :new.prenomstagiaire := trim(initcap(:new.prenomstagiaire));
    :new.emailstagiaire := trim(lower(:new.emailstagiaire));
    
    -- V�rifie si le nom du stagiaire est dans un format correct
    if (not regexp_like(:new.nomstagiaire, '^[a-z]+$', 'i')) then
      raise_application_error(-20150, 'Veuillez saisir un nom de stagiaire correct');
    elsif (not regexp_like(:new.prenomstagiaire, '^[a-z]+$', 'i')) then -- V�rifie si le pr�nom du stagiaire est dans un format correct
      raise_application_error(-20151, 'Veuillez saisir un pr�nom de stagiaire correct');
    end if;
    
    -- V�rifie si un stagiaire porte d�j� le couple nom/pr�nom
    select count(*) into vNbValue
      from stagiaire
        where nomstagiaire = :new.nomstagiaire and prenomstagiaire = :new.prenomstagiaire;
    if (vNbValue = 1) then
      raise_application_error(-20152, 'Ce stagiaire est d�j� enregistrer');
    end if;
    
    -- V�rifie si l'email est correct et si elle est d�j� utilis� par un stagiaire
    select count(*) into vNbValue
      from stagiaire
        where emailstagiaire = :new.emailstagiaire;
    if (vNbValue = 1) then
      raise_application_error(-20153, 'Cette email est d�j� utilis�e par un stagiaire');
    elsif (not regexp_like (:new.emailstagiaire, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
      raise_application_error(-20154, 'Le format de l''adresse email est incorrect');
    end if;
    
    -- V�rifie si le num�ro de t�l�phone est correct et s'il est d�j� utilis� par un stagiaire
    select count(*) into vNbValue
      from stagiaire
        where telstagiaire = :new.telstagiaire;
    if (vNbValue = 1) then
      raise_application_error(-20155, 'Ce num�ro de t�l�phone est d�j� utilis� par un stagiaire');
    elsif (length(:new.telstagiaire) != 10 or not regexp_like(:new.telstagiaire, '^[0-9]+$')) then
      raise_application_error(-20156, 'Le format du num�ro de t�l�phone est incorrect');
    end if;
    
    -- V�rifie l'adresse
    if (not regexp_like(:new.villestagiaire, '^[a-z]+$', 'i')) then
      raise_application_error(-20157, 'La ville du stagiaire a un format incorrect');
    elsif (length(:new.codepostalstagiaire) != 5 or not regexp_like(:new.codepostalstagiaire, '^[0-9]+$')) then
      raise_application_error(-20158, 'Le code postal n''a pas un bon format');
    elsif (not regexp_like(:new.nomruestagiaire, '^[a-z ]+$', 'i')) then
      raise_application_error(-20159, 'Le nom de rue du stagiaire a un format incorrect');
    elsif (length(:new.numruestagiaire) > 999 or not regexp_like(:new.numruestagiaire, '^[0-9]+$')) then
      raise_application_error(-20160, 'Le num�ro de rue du stagiaire a un format incorrect');
    end if;
    
    -- D�fini l'identifiant du stagiaire    
    select count(*) + 1 into vNbValue
      from stagiaire;
    :new.idstagiaire := vNbValue;
  end if;
end triggerStagiaire;
/

--------------------------------------------------------------------------------
-- PROCEDURE
--------------------------------------------------------------------------------


/*
 * Proc�dure qui modifie un stagiaire
 * @author Phil'Dy Belcou
 */
create or replace procedure modifierStagiaire(nom varchar2, prenom varchar2, sexe char, email varchar2, 
                                              tel varchar2, ville varchar2, code varchar2, nomrue varchar2, numrue varchar2, pId int) is
  vNbValue int;
begin

  -- V�rifie si le nom du stagiaire est dans un format correct
  if (not regexp_like(nom, '^[a-z]+$', 'i')) then
    raise_application_error(-20150, 'Veuillez saisir un nom de stagiaire correct');
  elsif (not regexp_like(prenom, '^[a-z]+$', 'i')) then -- V�rifie si le pr�nom du stagiaire est dans un format correct
    raise_application_error(-20151, 'Veuillez saisir un pr�nom de stagiaire correct');
  end if;
  
  -- V�rifie si un stagiaire porte d�j� le couple nom/pr�nom
  select count(*) into vNbValue
    from stagiaire
      where nomstagiaire = nom and prenomstagiaire = prenom and idstagiaire != pId;
  if (vNbValue = 1) then
    raise_application_error(-20152, 'Ce stagiaire est d�j� enregistrer');
  end if;
  
  -- V�rifie si l'email est correct et si elle est d�j� utilis� par un stagiaire
  select count(*) into vNbValue
    from stagiaire
      where emailstagiaire = email and idstagiaire != pId;
  if (vNbValue = 1) then
    raise_application_error(-20153, 'Cette email est d�j� utilis�e par un stagiaire');
  elsif (not regexp_like (email, '^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')) then
    raise_application_error(-20154, 'Le format de l''adresse email est incorrect');
  end if;
  
  -- V�rifie si le num�ro de t�l�phone est correct et s'il est d�j� utilis� par un stagiaire
  select count(*) into vNbValue
    from stagiaire
      where telstagiaire = tel and idstagiaire != pId;
  if (vNbValue = 1) then
    raise_application_error(-20155, 'Ce num�ro de t�l�phone est d�j� utilis� par un stagiaire');
  elsif (length(tel) != 10 or not regexp_like(tel, '^[0-9]+$')) then
    raise_application_error(-20156, 'Le format du num�ro de t�l�phone est incorrect');
  end if;
  
  -- V�rifie l'adresse
  if (not regexp_like(ville, '^[a-z]+$', 'i')) then
    raise_application_error(-20157, 'La ville du stagiaire a un format incorrect');
  elsif (length(code) != 5 or not regexp_like(code, '^[0-9]+$')) then
    raise_application_error(-20158, 'Le code postal n''a pas un bon format');
  elsif (not regexp_like(nomrue, '^[a-z ]+$', 'i')) then
    raise_application_error(-20159, 'Le nom de rue du stagiaire a un format incorrect');
  elsif (length(numrue) > 999 or not regexp_like(numrue, '^[0-9]+$')) then
    raise_application_error(-20160, 'Le num�ro de rue du stagiaire a un format incorrect');
  end if;
  
  update stagiaire 
    set nomstagiaire = nom,
        prenomstagiaire = prenom,
        sexestagiaire = sexe,
        emailstagiaire = email,
        telstagiaire = tel,
        villestagiaire = ville,
        codepostalstagiaire = code,
        nomruestagiaire = nomrue,
        numruestagiaire = numrue
    where idstagiaire = pId;
end modifierStagiaire;
/


--------------------------------------------------------------------------------
-- JEUX D'ESSAI
--------------------------------------------------------------------------------

/*
 * Jeux d'essais de la table formateur
 * @author Phil'Dy Belcou
 */
delete from stagiaire;

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(1, 'Pavin', 'Marcelino', 'H', 'pavin@hotmail.fr', '0685347825', 'Toulouse', 31200, 'Rue de la brioche', 5);
  
insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(2, 'Gardin', 'Jos�phine', 'F', 'gardin@hotmail.fr', '0641278423', 'Caen', 14000, 'Rue du magicien', 1);

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(3, 'Snow', 'John', 'H', 'snow@hotmail.fr', '0635412792', 'Paris', 75001, 'Rue du pain confiture', 15);

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(4, 'Moss', 'Angela', 'F', 'moss@hotmail.fr', '0631478256', 'Rouen', 76000, 'Rue de la belle rue', 2);

insert into stagiaire(idstagiaire, nomstagiaire, prenomstagiaire, sexestagiaire, emailstagiaire, 
                      telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) 
  values(5, 'Leslie', 'Rose', 'F', 'leslie@hotmail.fr', '0682924578', 'Paris', 75001, 'Rue du pain confiture', 15);
commit;