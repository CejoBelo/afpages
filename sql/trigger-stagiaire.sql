-- Autheur : Phil'Dy Belcou
create or replace trigger ajoutStagiaire
before insert or delete on stagiaire
for each row
declare
  vNbLines int;
  vNbValue int;
begin
  if (inserting) then
    :new.nomstagiaire := trim(initcap(:new.nomstagiaire));
    :new.prenomstagiaire := trim(initcap(:new.prenomstagiaire));
    :new.emailstagiaire := trim(lower(:new.emailstagiaire));
    
    checkStagiaire(:new.nomstagiaire, :new.prenomstagiaire, :new.emailstagiaire, :new.telstagiaire);
    
    select count(*) + 1 into vNbLines
      from stagiaire;
    :new.idstagiaire := vNbLines;
  /*elsif (deleting) then
    select count(*) into vNbValue
      from sessions s 
        where s.idstagiaire = :old.idstagiaire;
    if (vNbValue > 0) then
      raise_application_error(-20105, 'Le formateur est affili� � une session');
    end if;*/
  end if;
end ajoutStagiaire;