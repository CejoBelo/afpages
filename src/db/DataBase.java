package db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

/*


•	Méthode initInfos() : Récupère les informations de connexion dans un fichier database.json à la racine du projet.
•	Méthode initDriver() : Initialise le chargement du driver Oracle
•	Méthode initConnection() : Initialise la connexion à la base de données
•	Méthode close() : Ferme la connexion

 */

public class DataBase {
	private static String driver;
	private static String url;
	private static String user;
	private static String pass;
	private static Connection con;

	public DataBase() {
		this.initInfos();
		this.initDriver();
		this.initConnection();
	}

	// Initialisation des informations de connexion
	private void initInfos() {

		this.driver = "oracle.jdbc.driver.OracleDriver";
		this.url = "jdbc:oracle:thin:@10.116.24.108:1521:DL2";
		this.user = "FILROUGE24pbe";
		this.pass = "afpa";
	}

	// Initialisation du driver
	private void initDriver() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// Initialisation de la connexion
	private void initConnection() {
		if (this.con == null) {
			try {
				this.con = DriverManager.getConnection(url, user, pass);
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Base de données indisponible.", "Erreur", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	// Fermeture de la connexion
	public void close() {
		try {
			this.con.close();
			this.con = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getCon() {
		return con;
	}
}