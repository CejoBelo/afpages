package listeners;

import manager.FormateurMgr;
import views.Login;
import views.Application;

import javax.swing.*;
import java.awt.event.*;
import java.sql.SQLException;

/**
 * Permet de gérer la validation du formulaire de la page de connexion avec la touche « Entrée » du clavier et le clique
 * sur le bouton « Connexion » ainsi que l’arrêt de l’application lors de la fermeture de la JDialog.
 * @author Phil'Dy Belcou
 */
public class LoginListener implements WindowListener, KeyListener, ActionListener {
    private Login login;

    public LoginListener(Login login) {
        this.login = login;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowActivated(WindowEvent e) {}
    @Override
    public void windowClosed(WindowEvent e) {}
    @Override
    public void windowDeactivated(WindowEvent e) {}
    @Override
    public void windowDeiconified(WindowEvent e) {}
    @Override
    public void windowIconified(WindowEvent e) {}
    @Override
    public void windowOpened(WindowEvent e) {}
    @Override
    public void keyReleased(KeyEvent e) {}
    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.login.getBtnConnexion()) {
            this.connection();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            this.connection();
        }
    }

    public void connection() {
        String strLogin = this.login.getTxtId().getText();
        String strMdp = this.login.getTxtMdp().getText();

        try {
            FormateurMgr.connection(strLogin, strMdp);
            Application.login();
            login.init();
        } catch (SQLException e) {
            login.error();
            if (e.getErrorCode() == 20500) {
                JOptionPane.showMessageDialog(null, "Identification incorrecte", "Erreur", JOptionPane.ERROR_MESSAGE);
            } else {
                e.printStackTrace();
            }
        }
    }
}
