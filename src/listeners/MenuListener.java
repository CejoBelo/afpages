package listeners;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import views.Application;
import views.Menu;

/**
 * Permet de gérer les cliques sur les différents boutons du menu pour afficher la page
 * @author Phil'Dy Belcou
 */
public class MenuListener implements ActionListener, MouseListener {
	private Menu menu;

	public MenuListener(Menu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		if (source == this.menu.getBtnDeconnexion()) {
			Application.logout();
		} else if (source == this.menu.getBtnFormation()) {
			Application.changePage("Formation");
		} else if (source == this.menu.getBtnModule()) {
			Application.changePage("Module");
		} else if (source == this.menu.getBtnSession()) {
			Application.changePage("Session");
		} else if (source == this.menu.getBtnEvaluation()) {
			Application.changePage("Evaluation");
		} else if (source == this.menu.getBtnFormateur()) {
			Application.changePage("Formateur");
		} else if (source == this.menu.getBtnStagiaire()) {
			Application.changePage("Stagiaire");
		} else if (source == this.menu.getBtnProfil()) {
			Application.changePage("Profil");
		}
	}

	@Override
	public void mouseEntered(MouseEvent mouseEvent) {
		JButton source = (JButton) mouseEvent.getSource();
		for (JButton btn : this.menu.getButtons()) {
			if (btn == source && source == this.menu.getBtnDeconnexion()) {
				btn.setBackground(new Color(192, 57, 43));
				btn.setForeground(Color.white);
			} else if (btn == source &&  source != this.menu.getBtnDeconnexion()
					&& source != this.menu.getBtnEvaluation() && source != this.menu.getBtnProfil()) {
				btn.setBackground(new Color(135, 187, 52));
				btn.setForeground(Color.white);
			}
		}
	}

	@Override
	public void mouseExited(MouseEvent mouseEvent) {
		JButton source = (JButton) mouseEvent.getSource();
		for (JButton btn : this.menu.getButtons()) {
			if (btn == source && source == this.menu.getBtnDeconnexion()) {
				btn.setBackground(new Color(231, 76, 60));
				btn.setForeground(Color.white);
			} else if (btn == source &&  source != this.menu.getBtnDeconnexion()) {
				btn.setBackground(Color.white);
				btn.setForeground(new Color(135, 187, 52));
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent mouseEvent) {}
	@Override
	public void mousePressed(MouseEvent mouseEvent) {}
	@Override
	public void mouseReleased(MouseEvent mouseEvent) {}
}
