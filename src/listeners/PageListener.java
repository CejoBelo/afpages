package listeners;

import views.Application;
import views.FormAction;
import views.pages.Page;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Permet de gérer la sélection d’une ligne dans un JTable, les cliques sur les boutons CRUD et les événements clavier
 * et souris des formulaires.
 * @author Phil'Dy Belcou
 */
public class PageListener implements ActionListener, ListSelectionListener {
    private Page page;

    public PageListener(Page page) {
        this.page = page;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();

        if (source == page.getBtnAjouter()) {
            page.go("Formulaire", "Ajouter");
        } else if (source == page.getBtnConsulter()) {
            page.go("Formulaire", "Consulter");
        } else if (source == page.getBtnModifier()) {
            page.go("Formulaire", "Modifier");
        } else if (source == page.getBtnSupprimer()) {
            page.go("Formulaire", "Supprimer");
        } else if (source == page.getFormSP().getBtnRetour()) {
            page.go("Principale");
        } else if (source == page.getFormSP().getBtnValider()) {
            if (page.getState() == FormAction.AJOUT)
                page.ajouter();
            else if (page.getState() == FormAction.MODIFICATION)
                page.modifier();
            else if (page.getState() == FormAction.SUPPRESSION)
                page.supprimer();
            else if (page.getState() == FormAction.CONSULTATION)
                page.go("Formulaire", "Modifier");
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
        page.getBtnModifier().setEnabled(true);
        page.getBtnConsulter().setEnabled(true);
        page.getBtnSupprimer().setEnabled(true);
        page.initSelected();
    }
}
