/**
 * @author : Phil'Dy Belcou
 */

package manager;

import db.DataBase;
import model.Formateur;
import oracle.jdbc.driver.OracleTypes;
import views.Application;
import views.User;

import java.sql.*;
import java.util.Vector;

/**
 * Manager de la page « Formateur »
 * @author
 */
public class FormateurMgr {
    /**
     * Ajoute un formateur dans la table
     * @param formateur Le formateur
     * @throws SQLException Généré si une erreur de saisie est détecté
     */
    public static void add(Formateur formateur) throws SQLException {
        DataBase db = new DataBase();
        PreparedStatement stm =
                db.getCon().prepareStatement("insert into formateur(nomformateur, prenomformateur, sexeformateur, emailformateur, telformateur, estadmin) " +
                        "values(?, ?, ?, ?, ?, ?)");
        stm.setString(1, formateur.getNom());
        stm.setString(2, formateur.getPrenom());
        stm.setString(3, formateur.getSexe() + "");
        stm.setString(4, formateur.getEmail());
        stm.setString(5, formateur.getTel());
        stm.setInt(6, formateur.getEstAdmin());
        stm.executeUpdate();

        stm.close();
        db.close();
    }

    /**
     * Modifie un formateur dans la table
     * @param formateur
     * @param id L'identifiant du formateur
     * @throws SQLException
     */
    public static void set(Formateur formateur, Object id) throws SQLException {
        DataBase db = new DataBase();
        PreparedStatement cs = db.getCon().prepareCall("{call modifierFormateur (?, ?, ?, ?, ?, ?, ?)}");
        cs.setString(1, formateur.getNom());
        cs.setString(2, formateur.getPrenom());
        cs.setString(3, formateur.getSexe() + "");
        cs.setString(4, formateur.getEmail());
        cs.setString(5, formateur.getTel());
        cs.setInt(6, formateur.getEstAdmin());
        cs.setInt(7, (Integer) id);
        cs.executeUpdate();

        db.close();
    }

    /**
     * Supprime un formateur dans la table
     * @param id L'identifiant du formateur
     * @throws SQLException
     */
    public static void delete(Object id) throws SQLException {
        DataBase db = new DataBase();
        PreparedStatement stm = db.getCon().prepareStatement("delete from formateur where idformateur = ?");
        stm.setInt(1, (Integer) id);
        stm.executeUpdate();
        db.close();
    }

    /**
     * Récupère la liste des formateurs dans la table
     * @return Retourne la liste des formateurs
     * @throws SQLException Généré si une erreur de saisie est détecté
     */
    public static Vector<Formateur> getList() throws SQLException {
        Vector<Formateur> listFormateur = new Vector<Formateur>();
        DataBase db = new DataBase();
        Statement stm = db.getCon().createStatement();
        ResultSet result = stm.executeQuery("select * from formateur order by idformateur");


        while (result.next()) {
            Formateur formateur = new Formateur(
                    result.getInt("idformateur"),
                    result.getString("nomformateur"),
                    result.getString("prenomformateur"),
                    result.getString("sexeformateur").charAt(0),
                    result.getString("emailformateur"),
                    result.getString("telformateur"),
                    result.getString("loginformateur"),
                    result.getInt("estAdmin")
            );
            listFormateur.add(formateur);
        }

        db.close();

        return listFormateur;
    }

    /**
     * Récupère un formateur dans la table
     * @param id L'identifiant du formateur
     * @return Retourne le formateur
     */
    public static Formateur get(Object id) {
        DataBase db = new DataBase();
        Formateur formateur = null;

        try {
            PreparedStatement stm = db.getCon().prepareStatement("select * from formateur where idformateur = ?");
            stm.setInt(1, (Integer) id);
            ResultSet result = stm.executeQuery();

            while (result.next()) {
                formateur = new Formateur(
                        result.getInt("idformateur"),
                        result.getString("nomformateur"),
                        result.getString("prenomformateur"),
                        result.getString("sexeformateur").charAt(0),
                        result.getString("emailformateur"),
                        result.getString("telformateur"),
                        result.getString("loginformateur"),
                        result.getInt("estadmin")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return formateur;
    }

    public static void connection(String login, String mdp) throws SQLException {
        DataBase db = new DataBase();

        CallableStatement cs = db.getCon().prepareCall("{call connection (?, ?, ?, ?, ?)}");
        cs.registerOutParameter (3, Types.CHAR);
        cs.registerOutParameter (4, Types.CHAR);
        cs.registerOutParameter (5, Types.CHAR);
        cs.setString(1, login);
        cs.setString(2, mdp);
        cs.execute();

        Application.setUser(FormateurMgr.getProfil(login));

        //db.close();
    }

    public static User getProfil(String login) {
        DataBase db = new DataBase();
        User user = null;

        try {
            CallableStatement cs = db.getCon().prepareCall("{call getProfil (?, ?)}");
            cs.registerOutParameter(2, OracleTypes.CURSOR);
            cs.setString(1, login);
            cs.executeUpdate();
            ResultSet result = (ResultSet) cs.getObject(2);

            while (result.next()) {
                user = new User(
                        result.getString("loginformateur"),
                        result.getString("nom"),
                        result.getString("droit"),
                        result.getString("sexe").charAt(0)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        db.close();

        return user;
    }
}
