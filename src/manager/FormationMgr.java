package manager;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import db.DataBase;
import model.Formation;
import oracle.jdbc.driver.OracleTypes;

/**
 *
 * @author t.lecostey
 *
 */

public class FormationMgr {

	/**
	 * Appele la procédure stockée getListFormation() qui récupére chaque formation
	 *  de la base de données et qui pour chaque, instancie un nouvel objet de type Formation et l'ajoute dans un vecteur
	 * @return Retourne la liste des formations
	 * @throws SQLException
	 */
	public static Vector<Formation> getListFormation() throws SQLException {
		Vector<Formation> listFormation = new Vector<Formation>();
		DataBase db = new DataBase();

		CallableStatement cs = db.getCon().prepareCall("{call getListFormation(?)}");
		cs.registerOutParameter(1, OracleTypes.CURSOR);
		cs.executeUpdate();

		ResultSet result = (ResultSet) cs.getObject(1);

		while (result.next()) {
			Formation formation = new Formation(
					result.getInt("id"),
					result.getString("nom"),
					result.getInt("duree")
					);
			listFormation.add(formation);
		}

		result.close();
		cs.close();
		db.close();

		return listFormation;
	}

	/**
	 * Récupére la formation sélectionnée
	 * @param nom : Prend en paramétre le nom de la formation sélectionnée par l'utilisateur
	 * @return
	 * @throws SQLException
	 */
	public static Formation getFormation(String nom) throws SQLException {
		DataBase db = new DataBase();
		Formation formation = null;

		CallableStatement cs = db.getCon().prepareCall("{call getFormation(?,?)}");
		cs.setString(1, nom);
		cs.registerOutParameter(2, OracleTypes.CURSOR);
		cs.executeUpdate();
		ResultSet result = (ResultSet) cs.getObject(2);

		while(result.next()) {
			formation = new Formation(
					result.getInt("id"),
					result.getString("nom"),
					result.getInt("duree")
					);
		}
		db.close();

		return formation;
	}

	/**
	 * Insére une nouvelle formation dans la base de données
	 * @param nom : Nom de la formation saisie par l'utilisateur
	 * @param nbHeure : Nombre d'heures saisie par l'utilisateur
	 * @throws SQLException
	 */
	public static void ajout(String nom, int nbHeure) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("insert into formation(NOMFORMATION, DURERFORMATION) values(?,?)");
		stm.setString(1, nom);
		stm.setInt(2, nbHeure);
		stm.executeUpdate();

		stm.close();
		db.close();
	}

	/**
	 * Modifie une formation selectionnée par l'utilisateur dans la base de données
	 * @param nom : Nom de la formation saisie par l'utilisateur
	 * @param nbHeure : Nombre d'heures de la formation saisie par l'utilisateur
	 * @throws SQLException
	 */
	public static void update(int id,String nom, int nbHeure) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("update formation set nomformation = ?, durerformation = ? where idformation = ?");
		stm.setString(1, nom);
		stm.setInt(2, nbHeure);
		stm.setInt(3, id);
		stm.executeUpdate();

		stm.close();
		db.close();
	}

	/**
	 * Supprime une formation selectionnée par l'utilisateur dans la base de données
	 * @param nom : Nom de la formation selectionnée
	 * @throws SQLException
	 */
	public static void supprimer(String nom) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("delete from formation where nomformation = ?");
		stm.setString(1, nom);
		stm.executeUpdate();

		stm.close();
		db.close();
	}
}