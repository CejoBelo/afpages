package manager;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import db.DataBase;
import model.Module;
import oracle.jdbc.driver.OracleTypes;

/**
 *
 * @author t.lecostey
 *
 */

public class ModuleMgr {

	/**
	 * Appele la procédure stockée getListModule qui récupére chaque module
	 *  de la base de données et qui pour chaque, instancie un nouvel objet de type Module
	 *  et l'ajoute dans un vecteur
	 * @return Retourne la liste des modules
	 * @throws SQLException
	 */
	public static Vector<Module> getListModule() throws SQLException {
		Vector<Module> listModule = new Vector<Module>();
		DataBase db = new DataBase();

		CallableStatement cs = db.getCon().prepareCall("{call getListModule(?)}");
		cs.registerOutParameter(1, OracleTypes.CURSOR);
		cs.executeUpdate();

		ResultSet result = (ResultSet) cs.getObject(1);

		while (result.next()) {
			Module module = new Module(
					result.getInt("idModule"),
					result.getString("nom"),
					result.getString("formation")
					);
			listModule.add(module);
		}

		result.close();
		cs.close();
		db.close();

		return listModule;
	}

	/**
	 *
	 * @param nom : Prend en paramétre le nom du module saisi par l'utilisateur
	 * @return
	 * @throws SQLException
	 */
	public static Module getModule(String nom) throws SQLException {
		DataBase db = new DataBase();
		Module module = null;

		CallableStatement cs = db.getCon().prepareCall("{call getModule(?,?)}");
		cs.setString(1, nom);
		cs.registerOutParameter(2, OracleTypes.CURSOR);
		cs.executeUpdate();
		ResultSet result = (ResultSet) cs.getObject(2);

		while(result.next()) {
			module = new Module(
					result.getInt("idModule"),
					result.getString("nom"),
					result.getString("formation")
					);
		}
		cs.close();
		db.close();

		return module;
	}

	/**
	 * Insére un nouveau module dans la base de données
	 * @param nom : Nom du module saisi par l'utilisateur
	 * @param idFormation : L'identifiant de la formation auquel le module est rattaché
	 * @throws SQLException
	 */
	public static void ajout(String nom, int idFormation) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("insert into module(NOMMODULE, IDFORMATION) values(?,?)");
		stm.setString(1, nom);
		stm.setInt(2, idFormation);
		stm.executeUpdate();

		stm.close();
		db.close();
	}

	/**
	 * Modifie un module selectionné par l'utilisateur dans la base de données
	 * @param nom : Nom du module saisi par l'utilisateur
	 * @param idFormation : L'identifiant de la formation auquel le module est rattaché
	 * @throws SQLException
	 */
	public static void update(int idModule,String nom, int idFormation) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("update module set nommodule = ? , idformation = ? where idmodule = ?");
		stm.setString(1, nom);
		stm.setInt(2, idFormation);
		stm.setInt(3, idModule);
		stm.executeUpdate();

		stm.close();
		db.close();
	}

	/**
	 * Supprime un module selectionné par l'utilisateur dans la base de données
	 * @param nom : Nom du module selectionné
	 * @throws SQLException
	 */
	public static void supprimer(String nom) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("delete from module where nommodule = ?");
		stm.setString(1, nom);
		stm.executeUpdate();

		stm.close();
		db.close();
	}
}