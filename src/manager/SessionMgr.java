package manager;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import db.DataBase;
import model.Session;
import oracle.jdbc.driver.OracleTypes;

/**
 *
 * @author t.lecostey
 *
 */

public class SessionMgr {

	/**
	 * Appele la procédure stockée getListSession() qui récupére chaque session
	 *  de la base de données et qui pour chaque, instancie un nouvel objet de type Session
	 *  et l'ajoute dans un vecteur
	 * @return Retourne la liste des sessions
	 * @throws SQLException
	 */
	public static Vector<Session> getListSession() throws SQLException {
		Vector<Session> listSession = new Vector<Session>();
		DataBase db = new DataBase();

		CallableStatement cs = db.getCon().prepareCall("{call getListSession(?)}");
		cs.registerOutParameter(1, OracleTypes.CURSOR);
		cs.executeUpdate();

		ResultSet result = (ResultSet) cs.getObject(1);

		while (result.next()) {
			Session session = new Session(
					result.getInt("idSession"),
					result.getString("nom"),
					result.getString("dateDebut"),
					result.getString("dateFin"),
					result.getString("formateur"),
					result.getString("formation")
					);
			listSession.add(session);
		}

		result.close();
		cs.close();
		db.close();

		return listSession;
	}

	/**
	 *
	 * @param nom : Prend en paramétre le nom de la session saisi par l'utilisateur
	 * @return
	 * @throws SQLException
	 */
	public static Session getSession(String nom) throws SQLException {
		DataBase db = new DataBase();
		Session session = null;

		CallableStatement cs = db.getCon().prepareCall("{call getSession(?,?)}");
		cs.setString(1, nom);
		cs.registerOutParameter(2, OracleTypes.CURSOR);
		cs.executeUpdate();
		ResultSet result = (ResultSet) cs.getObject(2);

		while(result.next()) {
			session = new Session(
					result.getInt("idSession"),
					result.getString("nom"),
					result.getString("dateDebut"),
					result.getString("dateFin"),
					result.getString("formateur"),
					result.getString("formation")
					);
		}
		cs.close();
		db.close();

		return session;
	}

	/**
	 * Insére une nouvelle session dans la base de données
	 * @param nom : Nom de la session saisi par l'utilisateur
	 * @param debutSession : La date de début de session saisi par l'utilisateur
	 * @param finSession : La date de fin de session saisi par l'utilisateur
	 * @param idFormateur : L'identifiant du formateur référent attaché à la session
	 * @param idFormation : L'identifiant de la formation auquel la session est rattachée
	 * @throws SQLException
	 */
	public static void ajout(String nom, String debutSession, String finSession, int idFormateur, int idFormation) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("insert into sessions(NOMSESSION, DEBUTSESSION,FINSESSION,IDFORMATEUR,IDFORMATION) values(?,?,?,?,?)");
		stm.setString(1, nom);
		stm.setString(2, debutSession);
		stm.setString(3, finSession);
		stm.setInt(4,idFormateur);
		stm.setInt(5,idFormation);
		stm.executeUpdate();

		stm.close();
		db.close();
	}

	/**
	 * Modifie une session selectionnée par l'utilisateur dans la base de données
	 * @param nom : Nom de la session saisie par l'utilisateur
	 * @param debutSession : La date de début de session saisie par l'utilisateur
	 * @param finSession : La date de fin de session saisie par l'utilisateur
	 * @param idFormateur : L'identifiant du formateur référent attaché à la session
	 * @param idFormation : L'identifiant de la formation auquel la session est rattachée
	 * @throws SQLException
	 */
	public static void update(int idSession, String nom, String debutSession, String finSession, int idFormateur, int idFormation) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("update sessions set NOMSESSION = ? , DEBUTSESSION = ?, FINSESSION = ?, IDFORMATEUR = ?, IDFORMATION = ? where IDSESSION = ?");
		stm.setString(1, nom);
		stm.setString(2, debutSession);
		stm.setString(3, finSession);
		stm.setInt(4,idFormateur);
		stm.setInt(5,idFormation);
		stm.setInt(6,idSession);
		stm.executeUpdate();

		stm.close();
		db.close();
	}

	/**
	 * Supprime une session selectionnée par l'utilisateur dans la base de données
	 * @param nom : Nom de la session selectionnée
	 * @throws SQLException
	 */
	public static void supprimer(String nom) throws SQLException {
		DataBase db = new DataBase();

		PreparedStatement stm = db.getCon().prepareStatement("delete from sessions where NOMSESSION = ?");
		stm.setString(1, nom);
		stm.executeUpdate();

		stm.close();
		db.close();
	}
}