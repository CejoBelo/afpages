package manager;

import db.DataBase;
import model.Stagiaire;
import oracle.jdbc.driver.OracleTypes;
import views.Application;
import views.User;

import java.sql.*;
import java.util.Vector;

/**
 * Manager de la page « Stagiaire »
 * @author Phil'Dy Belcou
 */
public class StagiaireMgr {
    /**
     * Ajoute un stagiaire dans la table
     * @param stagiaire Le stagiaire à ajouter
     * @throws SQLException Généré si une erreur de saisie est détecté
     */
    public static void add(Stagiaire stagiaire) throws SQLException {
        DataBase db = new DataBase();
        PreparedStatement stm =
                db.getCon().prepareStatement("insert into " +
                        "stagiaire(nomstagiaire, prenomstagiaire, sexestagiaire, " +
                        "emailstagiaire, telstagiaire, villestagiaire, codepostalstagiaire, nomruestagiaire, numruestagiaire) " +
                        "values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
        stm.setString(1, stagiaire.getNom());
        stm.setString(2, stagiaire.getPrenom());
        stm.setString(3, stagiaire.getSexe() + "");
        stm.setString(4, stagiaire.getEmail());
        stm.setString(5, stagiaire.getTel());
        stm.setString(6, stagiaire.getVille());
        stm.setString(7, stagiaire.getCodePostal());
        stm.setString(8, stagiaire.getNomRue());
        stm.setString(9, stagiaire.getNumRue());
        stm.executeUpdate();

        stm.close();
        db.close();
    }

    /**
     * Modifie un stagiaire dans la table
     * @param stagiaire
     * @param id L'identifiant du stagiaire
     * @throws SQLException
     */
    public static void set(Stagiaire stagiaire, Object id) throws SQLException {
        DataBase db = new DataBase();
        PreparedStatement stm = db.getCon().prepareStatement("{call modifierStagiaire (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
        stm.setString(1, stagiaire.getNom());
        stm.setString(2, stagiaire.getPrenom());
        stm.setString(3, stagiaire.getSexe() + "");
        stm.setString(4, stagiaire.getEmail());
        stm.setString(5, stagiaire.getTel());
        stm.setString(6, stagiaire.getVille());
        stm.setString(7, stagiaire.getCodePostal());
        stm.setString(8, stagiaire.getNomRue());
        stm.setString(9, stagiaire.getNumRue());
        stm.setInt(10, (Integer) id);
        stm.executeUpdate();

        db.close();
    }

    /**
     * Supprime un stagiaire dans la table
     * @param id L'identifiant du stagiaire
     * @throws SQLException
     */
    public static void delete(Object id) throws SQLException {
        DataBase db = new DataBase();
        PreparedStatement stm = db.getCon().prepareStatement("delete from stagiaire where idstagiaire = ?");
        stm.setInt(1, (Integer) id);
        stm.executeUpdate();
        db.close();
    }

    /**
     * Récupère la liste des stagiaires dans la table
     * @return Retourne la liste des stagiaires
     * @throws SQLException Généré si une erreur de saisie est détecté
     */
    public static Vector<Stagiaire> getList() throws SQLException {
        Vector<Stagiaire> listStagiaire = new Vector<Stagiaire>();
        DataBase db = new DataBase();
        Statement stm = db.getCon().createStatement();
        ResultSet result = stm.executeQuery("select * from stagiaire order by idstagiaire");


        while (result.next()) {
            Stagiaire stagiaire = new Stagiaire(
                    result.getInt("idstagiaire"),
                    result.getString("nomstagiaire"),
                    result.getString("prenomstagiaire"),
                    result.getString("sexestagiaire").charAt(0),
                    result.getString("emailstagiaire"),
                    result.getString("telstagiaire"),
                    result.getString("villestagiaire"),
                    result.getString("codepostalstagiaire"),
                    result.getString("nomruestagiaire"),
                    result.getString("numruestagiaire")
            );
            listStagiaire.add(stagiaire);
        }

        db.close();

        return listStagiaire;
    }

    /**
     * Récupère un stagiaire dans la table
     * @param id L'identifiant du stagiaire
     * @return Retourne le stagiaire
     */
    public static Stagiaire get(Object id) {
        DataBase db = new DataBase();
        Stagiaire stagiaire = null;

        try {
            PreparedStatement stm = db.getCon().prepareStatement("select * from stagiaire where idstagiaire = ?");
            stm.setInt(1, (Integer) id);
            ResultSet result = stm.executeQuery();

            while (result.next()) {
                stagiaire = new Stagiaire(
                        result.getInt("idstagiaire"),
                        result.getString("nomstagiaire"),
                        result.getString("prenomstagiaire"),
                        result.getString("sexestagiaire").charAt(0),
                        result.getString("emailstagiaire"),
                        result.getString("telstagiaire"),
                        result.getString("villestagiaire"),
                        result.getString("codepostalstagiaire"),
                        result.getString("nomruestagiaire"),
                        result.getString("numruestagiaire")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return stagiaire;
    }
}
