package model;

/**
 * Modèle de la table Formateur
 * @author Phil'Dy Belcou
 */
public class Formateur {
    int id;
    String nom;
    String prenom;
    char sexe;
    String email;
    String tel;
    String login;
    String mdp;
    int estAdmin;

    public Formateur(String nom, String prenom, char sexe, String email, String tel, int estAdmin) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.sexe = sexe;
        this.setEmail(email);
        this.setTel(tel);
        this.setEstAdmin(estAdmin);
    }

    public Formateur(int id, String nom, String prenom, char sexe, String email, String tel, String login, int estAdmin) {
        this.setId(id);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.sexe = sexe;
        this.setEmail(email);
        this.setTel(tel);
        this.setLogin(login);
        this.setEstAdmin(estAdmin);
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getTel() {
        return tel;
    }

    public String getLogin() {
        return login;
    }

    public String getMdp() {
        return mdp;
    }

    public int isBlAdmin() {
        return estAdmin;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public void setEstAdmin(int estAdmin) {
        this.estAdmin = estAdmin;
    }

    public int getEstAdmin() {
        return estAdmin;
    }

    public Object[] getLine() {
        return new Object[]{id, nom, prenom, sexe};
    }

    public char getSexe() {
        return sexe;
    }
}
