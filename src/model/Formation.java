package model;

/**
 *
 * @author t.lecostey
 *
 */

public class Formation {
	int id;
	String nom;
	int duree;

	/**
	 * Constructeur d'une nouvelle Formation
	 *
	 * @param id : Identifiant de la formation
	 * @param nom : Nom de la formation
	 * @param duree : Durée de la formation en heures
	 */
	public Formation(int id, String nom, int duree) {
		this.id = id;
		this.nom = nom;
		this.duree = duree;
	}

	public int getId() {
		return this.id;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDuree() {
		return this.duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public Object[] getLine() {
		return new String[]{String.valueOf(this.id), this.nom, String.valueOf(this.duree)};
	}
}
