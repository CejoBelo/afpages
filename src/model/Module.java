package model;

/**
 *
 * @author t.lecostey
 *
 */

public class Module {
	int idModule;
	String nom;
	String formation;

	/**
	 * Constructeur d'un nouveau module
	 *
	 * @param idModule : Identifiant du module
	 * @param nom : Nom du module
	 * @param formation : La formation auquel le module est rattaché
	 */
	public Module(int idModule, String nom, String formation) {
		this.idModule = idModule;
		this.nom = nom;
		this.formation = formation;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIdModule() {
		return this.idModule;
	}

	public void setIdModule(int idModule) {
		this.idModule = idModule;
	}

	public String getFormation() {
		return this.formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public Object[] getLine() {
		return new String[]{String.valueOf(this.idModule), this.nom, this.formation};
	}
}