package model;

/**
 *
 * @author t.lecostey
 *
 */

public class Session {
	int idSession;
	String nom;
	String dateDebut;
	String dateFin;
	String formateur;
	String formation;

	/**
	 * Constructeur d'une nouvelle Session
	 *
	 * @param idSession : Identifiant de la session
	 * @param nom : Nom de la session
	 * @param dateDebut : Date de debut de la session
	 * @param dateFin : Date de fin de la session
	 * @param formateur : Le formateur référent attaché a la session
	 * @param formation : La formation auquel la session est rattachée
	 */
	public Session(int idSession, String nom, String dateDebut, String dateFin, String formateur, String formation) {
		this.idSession = idSession;
		this.nom = nom;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.formateur = formateur;
		this.formation = formation;
	}

	public int getIdSession() {
		return this.idSession;
	}

	public void setIdSession(int idSession) {
		this.idSession = idSession;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDateDebut() {
		return this.dateDebut;
	}

	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}

	public String getDateFin() {
		return this.dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	public String getFormateur() {
		return this.formateur;
	}

	public void setFormateur(String formateur) {
		this.formateur = formateur;
	}

	public String getFormation() {
		return this.formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public Object[] getLine() {
		return new String[]{String.valueOf(this.idSession), this.nom,this.dateDebut, this.dateFin,this.formateur, this.formation};
	}
}