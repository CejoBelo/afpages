package model;

import java.util.Vector;

/**
 * Modèle de la table Stagiaire
 * @author Phil'Dy Belcou
 */
public class Stagiaire {
    int id;
    String nom;
    String prenom;
    char sexe;
    String tel;
    String email;
    String ville;
    String codePostal;
    String nomRue;
    String numRue;

    public Stagiaire(String nom, String prenom, char sexe, String email, String tel, String ville, String codePostal, String nomRue, String numRue) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.sexe = sexe;
        this.setEmail(email);
        this.setTel(tel);
        this.setVille(ville);
        this.setCodePostal(codePostal);
        this.setNomRue(nomRue);
        this.setNumRue(numRue);
    }

    public Stagiaire(int id, String nom, String prenom, char sexe, String email, String tel, String ville, String codePostal, String nomRue, String numRue) {
        this.setId(id);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.sexe = sexe;
        this.setEmail(email);
        this.setTel(tel);
        this.setVille(ville);
        this.setCodePostal(codePostal);
        this.setNomRue(nomRue);
        this.setNumRue(numRue);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public String getNumRue() {
        return numRue;
    }

    public void setNumRue(String numRue) {
        this.numRue = numRue;
    }

    public Object[] getLine() {
        return new Object[]{id, nom, prenom, sexe};
    }
}
