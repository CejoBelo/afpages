package views;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import views.pages.Page;

/**
 * Gère la connexion et la déconnexion, le changement de page et sous-page et sert de « base » aux instances des classes
 * de l’application.
 * @author Phil'Dy Belcou
 */
public class Application extends JFrame {
	private static Login login;
	private static Menu menu;
	private static Content content;
	private static JPanel pan;
	private static Application jframe;
	private static Page page;
	public static User user;

	public Application() {
		this.setSize(800, 600);

		this.setResizable(false);
		this.setTitle("AfpaGes");
		this.setIconImage(new ImageIcon(this.getClass().getResource("/images/icone.png")).getImage());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());
		this.setVisible(false);

		login = new Login(this);
		pan = (JPanel) this.getContentPane();
		jframe = this;
		login.setVisible(true);
	}

	/**
	 * Connecte l'utilisateur
	 */
	public static void login() {
		login.setVisible(false);
		jframe.setVisible(true);
		menu = new Menu();
		content = new Content();
		content.getPages().get("Accueil").go("Principale");
		pan.add(menu, BorderLayout.WEST);
		pan.add(content, BorderLayout.CENTER);
		pan.updateUI();
	}

	/**
	 * Déconnecte l'utilisateur
	 */
	public static void logout() {
		jframe.setVisible(false);
		login.setVisible(true);
		pan.removeAll();
	}

	/**
	 * Change la page
	 * @param name le nom de la page
	 */
	public static void changePage(String name) {
		content.getCardLayout().show(content, name);
		content.getPages().get(name).go("Principale");
		content.updateUI();
	}

	/**
	 * CHange la sous-page
	 * @param page le nom de la page
	 * @param name le nom de la sous-page
	 */
	public static void changeSubPage(Page page, String name) {
		page.getCardLayout().show(page.getPnlSubPages(), name);

		if (name.equals("Formulaire")) {
			page.initForm();
		} else if (name.equals("Principale")) {
			page.getList();
		}

		page.updateUI();
	}

	public static Page getPage() {
		return page;
	}

	public static void setUser(User newProfil) {
		user = newProfil;
	}

	public static User getUser() {
		return user;
	}

	public static Application getJframe() {
		return jframe;
	}
}
