package views;

import java.awt.CardLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import views.pages.AccueilPage;
import views.pages.FormateurPage;
import views.pages.FormationPage;
import views.pages.ModulePage;
import views.pages.Page;
import views.pages.SessionPage;
import views.pages.StagiairePage;

/**
 * Contient les instances des pages de l’application, les affiches par rapport aux cliques sur les boutons
 * @author Phil'Dy Belcou
 */
public class Content extends JPanel {
	private CardLayout cardLayout;

	private Map<String, Page> pages = new HashMap<String, Page>();
	private Page accueil;
	private Page formation;
	private Page module;
	private Page session;
	private Page formateur;
	private Page stagiaire;

	public Content() {
		this.cardLayout = new CardLayout();
		this.setLayout(this.cardLayout);

		this.pages.put("Accueil", this.accueil = new AccueilPage());
		this.add(this.accueil, "Accueil");

		this.pages.put("Formation", this.formation = new FormationPage());
		this.add(this.formation, "Formation");

		this.pages.put("Module", this.module = new ModulePage());
		this.add(this.module, "Module");

		this.pages.put("Session", this.session = new SessionPage());
		this.add(this.session, "Session");

		this.pages.put("Formateur", this.formateur = new FormateurPage());
		this.add(this.formateur, "Formateur");

		this.pages.put("Stagiaire", this.stagiaire = new StagiairePage());
		this.add(this.stagiaire, "Stagiaire");
	}

	public CardLayout getCardLayout() {
		return this.cardLayout;
	}

	public Map<String, Page> getPages() {
		return this.pages;
	}
}
