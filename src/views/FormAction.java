package views;

/**
 * Énumération qui permet de savoir sur quel formulaire on se trouve
 * @author Phil'Dy Belcou
 */
public enum FormAction {
    MODIFICATION, SUPPRESSION, CONSULTATION, AJOUT
}
