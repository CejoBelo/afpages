package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import listeners.LoginListener;


/**
 * Fenêtre de connexion
 * @author Phil'Dy Belcou
 */
public class Login extends JDialog {
	private Application application;
	private JPanel content;
	private JPanel pnlForm;
	private JLabel image;
	private JLabel lblId;
	private JLabel lblMdp;
	private JTextField txtId;
	private JPasswordField txtMdp;
	private JButton btnConnexion;

	public Login(Application application) {
		super(application, "AfpaGes - Connexion");

		this.setSize(300, 450);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.setIconImage(new ImageIcon(this.getClass().getResource("/images/icone.png")).getImage());

		this.application = application;
		this.initControls();
	}

	/**
	 * Initialise les contrôles
	 */
	private void initControls() {
		this.content = (JPanel) this.getContentPane();
		this.content.setLayout(new BorderLayout());

		this.image = new JLabel(new ImageIcon(this.getClass().getResource("/images/logo.png")), JLabel.CENTER);
		this.pnlForm = new JPanel();
		this.lblId = new JLabel("Identifiant");
		this.txtId = new JTextField(15);
		this.lblMdp = new JLabel("Mot de passe");
		this.txtMdp = new JPasswordField(15);
		JPanel btn_wrapper = new JPanel();
		this.btnConnexion = new JButton("Se connecter");

		this.image.setBackground(Color.WHITE);
		this.image.setBorder(BorderFactory.createEmptyBorder(15, 30, 15, 30));
		this.image.setOpaque(true);

		this.pnlForm.setLayout(new GridLayout(4, 1));
		this.pnlForm.setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 30));
		this.pnlForm.setBackground(Color.WHITE);
		this.lblId.setForeground(Color.DARK_GRAY);
		this.txtId.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY));
		this.txtId.setBorder(BorderFactory.createCompoundBorder(
				this.txtId.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);
		this.lblMdp.setForeground(Color.DARK_GRAY);
		this.txtMdp.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY));
		this.txtMdp.setBorder(BorderFactory.createCompoundBorder(
				this.txtMdp.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);

		this.btnConnexion.setPreferredSize(new Dimension(0, 40));
		this.btnConnexion.setBackground(new Color(97, 191, 26));
		this.btnConnexion.setForeground(Color.WHITE);
		btn_wrapper.setLayout(new GridLayout(1, 1));
		btn_wrapper.setBorder(BorderFactory.createEmptyBorder(15, 30, 15, 30));
		btn_wrapper.setBackground(Color.WHITE);

		this.pnlForm.add(this.lblId);
		this.pnlForm.add(this.txtId);
		this.pnlForm.add(this.lblMdp);
		this.pnlForm.add(this.txtMdp);
		btn_wrapper.add(this.btnConnexion);
		this.content.add(this.image, BorderLayout.NORTH);
		this.content.add(this.pnlForm, BorderLayout.CENTER);
		this.content.add(btn_wrapper, BorderLayout.SOUTH);

		this.addWindowListener(new LoginListener(this));
		this.btnConnexion.addActionListener(new LoginListener(this));
		this.txtId.addKeyListener(new LoginListener(this));
		this.txtMdp.addKeyListener(new LoginListener(this));
	}

	/**
	 * Met une bordure rouge sur les champs
	 */
	public void error() {
		this.init();
		this.txtId.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(231, 76, 60)));
		this.txtId.setBorder(BorderFactory.createCompoundBorder(
				this.txtId.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);
		this.txtMdp.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(231, 76, 60)));
		this.txtMdp.setBorder(BorderFactory.createCompoundBorder(
				this.txtMdp.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);
	}

	/**
	 * Initialise les champs du formulaire
	 */
	public void init() {
		this.txtId.setText("");
		this.txtMdp.setText("");
		this.txtId.requestFocus();

		this.txtId.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY));
		this.txtId.setBorder(BorderFactory.createCompoundBorder(
				this.txtId.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);

		this.txtMdp.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY));
		this.txtMdp.setBorder(BorderFactory.createCompoundBorder(
				this.txtMdp.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);
	}

	public JButton getBtnConnexion() {
		return this.btnConnexion;
	}

	public JTextField getTxtId() {
		return this.txtId;
	}

	public JTextField getTxtMdp() {
		return this.txtMdp;
	}
}