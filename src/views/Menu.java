package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import listeners.MenuListener;

/**
 * Menu de l'application
 * @author Phil'Dy Belcou
 */
public class Menu extends JPanel {
	private JPanel parentContent;

	private Vector<JButton> buttons = new Vector<JButton>();
	private JButton btnFormation;
	private JButton btnModule;
	private JButton btnSession;
	private JButton btnEvaluation;
	private JButton btnFormateur;
	private JButton btnStagiaire;
	private JButton btnProfil;
	private JButton btnDeconnexion;

	public Menu() {
		this.setSize(400, 200);
		this.setBackground(Color.WHITE);
		this.setOpaque(true);
		this.setLayout(new GridLayout(2, 1));
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));

		this.initControls();
	}

	/**
	 * Initialise les composants du menu
	 */
	private void initControls() {
		JPanel pnlHeader = new JPanel();
		pnlHeader.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.gridx = 0;
		gc.weightx = 10;
		gc.weighty = 5;
		gc.fill = GridBagConstraints.HORIZONTAL;

		pnlHeader.setBackground(Color.WHITE);
		pnlHeader.add(new JLabel(new ImageIcon(this.getClass().getResource("/images/logo.png")), JLabel.CENTER), gc);
		pnlHeader.add(new JLabel(new ImageIcon(this.getClass().getResource("/images/profile-" + Application.user.getSexe() + ".png")), JLabel.CENTER), gc);
		gc.weighty = 2;
		String admin = "";
		if (Application.user.getDroit().equals("Administrateur")) {
			admin = " (Admin)";
		}
		pnlHeader.add(new JLabel(Application.user.getNom() + admin, JLabel.CENTER), gc);
		gc.insets = new Insets(5, 0, 0, 0);

		this.buttons.add(this.btnFormation = new JButton("Formation"));
		this.buttons.add(this.btnModule = new JButton("Module"));
		this.buttons.add(this.btnSession = new JButton("Session"));
		this.buttons.add(this.btnEvaluation = new JButton("Évaluation"));
		this.buttons.add(this.btnStagiaire = new JButton("Stagiaire"));
		this.buttons.add(this.btnFormateur = new JButton("Formateur"));
		this.buttons.add(this.btnProfil = new JButton("Statistiques"));
		this.buttons.add(this.btnDeconnexion = new JButton("Déconnexion"));

		JPanel pnlButtons = new JPanel(new GridLayout(8, 1, 0, 0));

		for (JButton btn : this.buttons) {
			btn.setBackground(Color.WHITE);
			btn.setForeground(new Color(135, 187, 52));
			btn.setOpaque(true);
			btn.setFont(new Font("Arial", Font.BOLD, 16));
			btn.setHorizontalAlignment(SwingConstants.LEFT);
			btn.setFocusPainted(false);
			btn.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.LIGHT_GRAY));
			btn.setBorder(BorderFactory.createCompoundBorder(
					btn.getBorder(),
					BorderFactory.createEmptyBorder(10, 10, 10, 10))
					);

			if (!Application.getUser().getDroit().equals("Administrateur") && btn.getText().equals("Formation") ||
					!Application.getUser().getDroit().equals("Administrateur") && btn.getText().equals("Module")	||
					!Application.getUser().getDroit().equals("Administrateur") && btn.getText().equals("Formateur") ||
					!Application.getUser().getDroit().equals("Administrateur") && btn.getText().equals("Profil") ||
					btn.getText().equals("Évaluation") || btn.getText().equals("Statistiques")) {
				btn.setEnabled(false);
			}

			pnlButtons.add(btn);
		}

		this.btnDeconnexion.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.LIGHT_GRAY));
		this.btnDeconnexion.setBorder(BorderFactory.createCompoundBorder(
				this.btnDeconnexion.getBorder(),
				BorderFactory.createEmptyBorder(10, 10, 10, 10))
				);

		this.btnDeconnexion.setBackground(new Color(231, 76, 60));
		this.btnDeconnexion.setForeground(Color.white);
		pnlButtons.setBackground(Color.white);

		this.add(pnlHeader);
		this.add(pnlButtons);

		for (JButton btn : this.buttons) {
			btn.addActionListener(new MenuListener(this));
			btn.addMouseListener(new MenuListener(this));
		}
	}

	public Vector<JButton> getButtons() { return this.buttons; }
	public JButton getBtnFormation() { return this.btnFormation; }
	public JButton getBtnModule() { return this.btnModule; }
	public JButton getBtnSession() { return this.btnSession; }
	public JButton getBtnEvaluation() { return this.btnEvaluation; }
	public JButton getBtnFormateur() { return this.btnFormateur; }
	public JButton getBtnStagiaire() { return this.btnStagiaire; }
	public JButton getBtnProfil() { return this.btnProfil; }
	public JButton getBtnDeconnexion() { return this.btnDeconnexion; }
}
