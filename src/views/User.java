package views;

/**
 * Classe qui contient quelques infos (nom, droit et sexe) du formateur connecté.
 * @author Phil'Dy Belcou
 */
public class User {
    String login;
    String nom;
    int estAdmin;
    String droit;
    char sexe;

    /**
     * Instancie l'utilisateur connecté
     * @param login Le login de l'utilisateur connecté
     * @param nom Le nom de l'utilisateur connecté
     * @param droit Le droit de l'utilisateur connecté
     * @param sexe Le sexe de l'utilisateur connecté
     */
    public User(String login, String nom, String droit, char sexe) {
        this.login = login;
        this.nom = nom;
        this.sexe = sexe;
        this.droit = droit;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDroit() {
        return droit;
    }

    public char getSexe() {
        return sexe;
    }

    public Object[] getLine() {
        return new String[]{login, nom, droit};
    }

    public String getLogin() {
        return login;
    }

    public int getEstAdmin() {
        return estAdmin;
    }
}
