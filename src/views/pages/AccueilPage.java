package views.pages;

import java.awt.BorderLayout;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author t.lecostey
 *
 */

public class AccueilPage extends Page {
	@Override
	public void initForm() {}

	@Override
	public void getList() {}

	@Override
	public void remplirForm() {}

	@Override
	public void controlSaisie() {

	}

	@Override
	public void ajouter() {}

	@Override
	public void modifier() {}

	@Override
	public void supprimer() {}

	@Override
	public void controlCode(SQLException e) {

	}

	public AccueilPage() {
		super("Accueil", "Bienvenue sur AfpaGes !", new String[]{"Id", "Nom", "Prénom", "Sexe"});
		JPanel pnl = new JPanel();
		pnl.setLayout(new BorderLayout(10,10));
		pnl.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));

		JLabel lblImage = new JLabel("",JLabel.CENTER);
		lblImage.setIcon(new ImageIcon(this.getClass().getResource("/images/afpa.png")));

		pnl.add(lblImage, BorderLayout.CENTER);

		JLabel lblText = new JLabel("Développer par Phil'Dy Belcou & Thomas Lecostey            AfpaGes v1.0 © 2017 ", JLabel.CENTER);

		pnl.add(lblText, BorderLayout.SOUTH);

		this.add(pnl);
	}
}