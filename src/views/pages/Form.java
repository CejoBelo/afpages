package views.pages;

import listeners.PageListener;

import javax.swing.*;
import java.awt.*;

public class Form extends JPanel{
    private JButton btnRetour;
    private JPanel pnlContent;
    private JLabel lblTitle;
    private JButton btnValider;
    public GridBagConstraints gc = new GridBagConstraints();

    public Form() {
        this.setLayout(new BorderLayout());
        JPanel pnlRetour = new JPanel(new BorderLayout());
        lblTitle = new JLabel("", JLabel.CENTER);

        lblTitle.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 75));
        pnlRetour.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
        pnlRetour.setBorder(BorderFactory.createCompoundBorder(
                pnlRetour.getBorder(),
                BorderFactory.createEmptyBorder(5, 15, 5, 15))
        );
        pnlRetour.setBackground(new Color(245, 246, 246));

        btnRetour = new JButton(new ImageIcon(this.getClass().getResource("/images/back.png")));
        btnRetour.setText("Retour");
        btnRetour.setBorder(BorderFactory.createEmptyBorder());
        btnRetour.setBackground(Color.WHITE);
        btnRetour.setBackground(new Color(245, 246, 246));
        btnRetour.setForeground(new Color(51, 51, 51));
        btnRetour.setFont(new Font("Arial", Font.BOLD, 13));

        btnValider = new JButton("Valider");
        btnValider.setBackground(new Color(97, 191, 26));

        this.pnlContent = new JPanel(new GridBagLayout());

        pnlRetour.add(btnRetour, BorderLayout.WEST);
        pnlRetour.add(lblTitle, BorderLayout.CENTER);
        this.add(pnlRetour, BorderLayout.NORTH);
        this.add(pnlContent, BorderLayout.CENTER);
    }

    public void initGc() {
        gc.weightx = 0;
        gc.weighty = 0;
        gc.gridx = 0;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.CENTER;
        gc.fill = GridBagConstraints.NONE;
    }

    public JPanel getContent() {
        return this.pnlContent;
    }

    public JButton getBtnRetour() {
        return btnRetour;
    }

    public JLabel getLblTitle() {
        return lblTitle;
    }

    public JButton getBtnValider() {
        return btnValider;
    }
}
