package views.pages;

import manager.FormateurMgr;
import model.Formateur;
import views.Application;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

/**
 * Classe qui gère l'affichage de la page « Formateur »
 * @author Phil'Dy Belcou
 */
public class FormateurPage extends Page {
    private JTextField txtNom;
    private JTextField txtPrenom;
    private ButtonGroup grpSexe;
    private JRadioButton optHomme;
    private JRadioButton optFemme;
    private JTextField txtEmail;
    private JTextField txtTel;
    private JCheckBox chkAdmin;

    public FormateurPage() {
        super("Formateur", "Gestion des formateurs", new String[]{"Id", "Nom", "Prénom", "Sexe"});
    }

    @Override
    public void getList() {
        while (this.getTableModel().getRowCount() > 0) {
            this.getTableModel().removeRow(0);
        }

        try {
            for (Formateur formateur : FormateurMgr.getList()) {
                this.getTableModel().addRow(formateur.getLine());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ajouter() {
        Formateur formateur = new Formateur(
                txtNom.getText(),
                txtPrenom.getText(),
                (optFemme.isSelected()) ? 'F' : 'H',
                txtEmail.getText(),
                txtTel.getText(),
                (chkAdmin.isSelected()) ? 1 : 0
        );

        controlSaisie();

        try {
            FormateurMgr.add(formateur);

            JOptionPane.showMessageDialog(null, "Le formateur a bien été inséré");
            this.go("Principale");
        } catch (SQLException e) {
            controlCode(e);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void modifier() {
        int estAdmin = chkAdmin.isSelected() ? 1 : 0;

        if ((int) getIdSelected() == 1 && estAdmin == 0) {
            JOptionPane.showMessageDialog(null, "Le big boss doit être administrateur !");
            return;
        }

        Formateur formateur = new Formateur(
                txtNom.getText(),
                txtPrenom.getText(),
                (optFemme.isSelected()) ? 'F' : 'H',
                txtEmail.getText(),
                txtTel.getText(),
                estAdmin
        );

        controlSaisie();

        try {
            FormateurMgr.set(formateur, getIdSelected());

            if (formateur.getNom().equals(Application.getUser().getNom().split(" ")[0]) && formateur.getPrenom().equals(Application.getUser().getNom().split(" ")[1])) {
                JOptionPane.showMessageDialog(null, "Le formateur a bien été modifié\nCette action vous concerne, veuillez vous reconnecter");
                Application.logout();
            } else {
                JOptionPane.showMessageDialog(null, "Le formateur a bien été modifié");
                this.go("Principale");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            controlCode(e);
        }
    }

    @Override
    public void supprimer() {
        if ((int) getIdSelected() == 1) {
            JOptionPane.showMessageDialog(null, "Le big boss ne peut pas être supprimé !");
            return;
        }

        try {
            FormateurMgr.delete(getIdSelected());
            JOptionPane.showMessageDialog(null, "Le formateur a bien été supprimé");
            this.go("Principale");
        } catch (SQLException e) {
            if (e.getErrorCode() == 20105) {
                JOptionPane.showMessageDialog(null, "Le formateur est affilié à une session", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

     @Override
    public void initForm() {
         txtNom = new JTextField(30);
         txtPrenom = new JTextField(30);
         grpSexe = new ButtonGroup();
         optFemme = new JRadioButton("Femme");
         optHomme = new JRadioButton("Homme");
         txtEmail = new JTextField(20);
         txtTel = new JTextField(20);
         chkAdmin = new JCheckBox("Est administrateur");
         GridBagConstraints gc = new GridBagConstraints();

         grpSexe.add(optFemme);
         grpSexe.add(optHomme);
         getFormSP().getContent().setBorder(BorderFactory.createEmptyBorder(50, 100, 50, 100));

         // Nom
         gc.gridx = 0;
         gc.gridy = 0;
         gc.anchor = GridBagConstraints.LINE_START;
         this.getFormSP().getContent().add(new JLabel("Nom"), gc);
         getFormSP().initGc();
         gc.gridy = 1;
         gc.fill = GridBagConstraints.BOTH;
         gc.weightx = 1;
         this.getFormSP().getContent().add(txtNom, gc);

         getFormSP().initGc();

         // Prenom
         gc.gridy = 2;
         gc.anchor = GridBagConstraints.LINE_START;
         this.getFormSP().getContent().add(new JLabel("Prenom"), gc);
         getFormSP().initGc();
         gc.gridy = 3;
         gc.fill = GridBagConstraints.BOTH;
         gc.weightx = 1;
         this.getFormSP().getContent().add(txtPrenom, gc);

         getFormSP().initGc();

         // Sexe
         JPanel pnlSexe = new JPanel(new FlowLayout());
         pnlSexe.add(optFemme);
         pnlSexe.add(optHomme);

         gc.gridy = 4;
         gc.weightx = 0;
         gc.anchor = GridBagConstraints.LINE_START;
         this.getFormSP().getContent().add(new JLabel("Sexe"), gc);
         getFormSP().initGc();
         gc.gridy = 5;
         this.getFormSP().getContent().add(pnlSexe, gc);

         getFormSP().initGc();

         // Email
         gc.gridy = 6;
         gc.weightx = 0;
         gc.anchor = GridBagConstraints.LINE_START;
         this.getFormSP().getContent().add(new JLabel("Adresse email"), gc);
         getFormSP().initGc();
         gc.gridy = 7;
         this.getFormSP().getContent().add(txtEmail, gc);

         getFormSP().initGc();

         // Telephone
         gc.gridy = 8;
         gc.weightx = 0;
         gc.anchor = GridBagConstraints.LINE_START;
         this.getFormSP().getContent().add(new JLabel("Téléphone"), gc);
         getFormSP().initGc();
         gc.gridy = 9;
         this.getFormSP().getContent().add(txtTel, gc);

         getFormSP().initGc();

         // Est admin
         gc.gridy = 10;
         gc.weightx = 0;
         gc.anchor = GridBagConstraints.LINE_START;
         this.getFormSP().getContent().add(chkAdmin, gc);

         // Bouton de validation
         gc.gridy = 11;
         this.getFormSP().getContent().add(this.getFormSP().getBtnValider(), gc);
         this.getFormSP().getContent().updateUI();
    }

    @Override
    public void remplirForm() {
        Formateur formateur = FormateurMgr.get(getIdSelected());

        txtNom.setText(formateur.getNom());
        txtPrenom.setText(formateur.getPrenom());

        if (formateur.getSexe() == 'F') {
            optFemme.setSelected(true);
        } else {
            optHomme.setSelected(true);
        }

        txtEmail.setText(formateur.getEmail());
        txtTel.setText(formateur.getTel());

        if (formateur.getEstAdmin() == 1) {
            chkAdmin.setSelected(true);
        }
    }

    @Override
    public void controlSaisie() {
        txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));

        if (txtNom.getText().length() == 0 || txtNom.getText().length() > 24) {
            txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un nom correct (24 caractères maximum)", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtPrenom.getText().length() == 0 || txtPrenom.getText().length() > 24) {
            txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un prénom correct (24 caractères maximum)", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (grpSexe.getSelection() == null) {
            JOptionPane.showMessageDialog(null, "Veuillez choisir le sexe", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtEmail.getText().length() == 0 || txtEmail.getText().length() > 24) {
            txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir une email correcte", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtTel.getText().length() == 0) {
            txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un numéro de téléphone correct", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

    @Override
    public void controlCode(SQLException e) {
        if (e.getErrorCode() == 20106) {
            txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un nom de formateur correct", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20107) {
            txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un prenom de formateur correct", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20100) {
            txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Ce formateur est déjà enregistrer", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20101) {
            txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Cette email est déjà utiliser par un autre formateur", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20102) {
            txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le format de l'adresse email est incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20103) {
            txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Ce numéro de téléphone est déjà utilisé par un formateur", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20104) {
            txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le format du numéro de téléphone est incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        e.printStackTrace();
    }
}