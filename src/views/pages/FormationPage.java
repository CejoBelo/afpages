package views.pages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import manager.FormationMgr;
import model.Formation;
import views.Application;

/**
 *
 * @author t.lecostey
 */

@SuppressWarnings("serial")
public class FormationPage extends Page {

	private Formation selected;
	private JTextField txtNom, txtNbHeure;

	public FormationPage() {
		super("Formation", "Gestion des formations", new String[]{"Identifiant", "Nom", "Duree en heures"});
	}

	/**
	 * Récupére chaque formation et l'ajoute dans la JTable grace à une boucle
	 */
	@Override
	public void getList() {
		while (this.getTableModel().getRowCount() > 0) {
			this.getTableModel().removeRow(0);
		}
		try {
			for (Formation formation : FormationMgr.getListFormation()) {
				this.getTableModel().addRow(formation.getLine());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pré-rempli les champs du formulaire
	 */
	@Override
	public void remplirForm() {
		String nom = this.selected.getNom();
		this.txtNom.setText(nom);

		int nbHeure = this.selected.getDuree();
		this.txtNbHeure.setText(String.valueOf(nbHeure));
	}

	@Override
	public void controlSaisie() {

	}

	/**
	 * Ajoute une formation à la base de données ainsi que dans la JTable
	 */
	@Override
	public void ajouter() {
		String nom = this.txtNom.getText();
		String nbHeure = this.txtNbHeure.getText();
		this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		this.txtNbHeure.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		try {
			FormationMgr.ajout(nom, new Integer(nbHeure));
			JOptionPane.showMessageDialog(null, "Ajout de la formation " + nom + " reussie !","", JOptionPane.INFORMATION_MESSAGE);
			Application.changeSubPage(this, "Principale");
			this.getList();
		} catch (SQLException e) {
			if(e.getErrorCode() == 1400) {
				JOptionPane.showMessageDialog(null,"Impossible d'ajouter une formation avec un champ vide ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.requestFocus();
			} else if(e.getErrorCode() == 12899) {
				JOptionPane.showMessageDialog(null,"La valeur maximale du champ est de 25 caracteres ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.requestFocus();
				this.txtNom.setText("");
				this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 20007) {
				JOptionPane.showMessageDialog(null, "Une formation existante porte deja ce nom, veuillez en choisir un autre !", "Erreur !" , JOptionPane.ERROR_MESSAGE);
				this.txtNom.setText("");
				this.txtNom.requestFocus();
				this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 20016) {
				JOptionPane.showMessageDialog(null, "Le nombre d'heure doit etre au minimum 140 (Environ 1 mois)","Erreur !", JOptionPane.ERROR_MESSAGE);
				this.txtNbHeure.setText("");
				this.txtNbHeure.requestFocus();
				this.txtNbHeure.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			}
		} catch (NumberFormatException e1) {
			JOptionPane.showMessageDialog(null, "Format incorrect, veuillez ecrire un nombre !","Erreur !",JOptionPane.ERROR_MESSAGE);
			this.txtNbHeure.setText("");
			this.txtNbHeure.requestFocus();
			this.txtNbHeure.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
		}
	}

	/**
	 * Modifie une formation dans la base de données ainsi que dans la JTable
	 */
	@Override
	public void modifier() {
		int id = this.selected.getId();
		String nom = this.txtNom.getText();
		String nbHeure = this.txtNbHeure.getText();
		this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		this.txtNbHeure.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		try {
			FormationMgr.update(id, nom, new Integer(nbHeure));
			if (new Integer(nbHeure) < 140) {
				JOptionPane.showMessageDialog(null, "Le nombre d'heure doit etre au minimum 140 (Environ 1 mois)","Erreur !", JOptionPane.ERROR_MESSAGE);
				this.txtNbHeure.setText("");
				this.txtNbHeure.requestFocus();
				this.txtNbHeure.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else {
				JOptionPane.showMessageDialog(null, "Modification reussie !","", JOptionPane.INFORMATION_MESSAGE);
				this.go("Principale");
				this.getList();
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1) {
				JOptionPane.showMessageDialog(null, "Une formation existante porte deja ce nom, veuillez en choisir un autre !", "Erreur !" , JOptionPane.ERROR_MESSAGE);
				this.txtNom.setText("");
				this.txtNom.requestFocus();
				this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 1407) {
				JOptionPane.showMessageDialog(null,"Impossible de mettre a jour la formation avec un champ vide ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.requestFocus();
			}
		} catch (NumberFormatException e1) {
			JOptionPane.showMessageDialog(null, "Format incorrect, veuillez ecrire un nombre !","Erreur !",JOptionPane.ERROR_MESSAGE);
			this.txtNbHeure.setText("");
			this.txtNbHeure.requestFocus();
			this.txtNbHeure.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
		}
	}

	/**
	 * Supprimer une formation dans la base de données ainsi que dans la JTable
	 */
	@Override
	public void supprimer() {
		try {
			FormationMgr.supprimer(this.selected.getNom());
			JOptionPane.showMessageDialog(null, "La formation " + this.selected.getNom() + " a bien été supprimee", "Suppression reussie", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
		} catch (SQLException e) {
			if(e.getErrorCode() == 20902) {
				JOptionPane.showMessageDialog(null, "La formation " +this.selected.getNom() + " est affiliee a au moins une session,"
						+ " impossible de la supprimee !","Erreur !",JOptionPane.ERROR_MESSAGE);
			} else if(e.getErrorCode() == 20905) {
				JOptionPane.showMessageDialog(null, "La formation " +this.selected.getNom() + " est affiliee a au moins un module,"
						+ " impossible de la supprimee !","Erreur !",JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void controlCode(SQLException e) {}

	/**
	 * Initialise le formulaire d'ajout ou de modification de formation
	 */
	@Override
	public void initForm() {
		GridBagConstraints gc = new GridBagConstraints();

		this.txtNom = new JTextField();
		this.txtNom.setPreferredSize(new Dimension(150,30));

		this.txtNbHeure = new JTextField();
		this.txtNbHeure.setPreferredSize(new Dimension(150,30));

		gc.gridx = 0;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;

		this.getFormSP().getContent().add(new JLabel("Nom de la formation :"), gc);
		gc.insets = new Insets(0, 125, 0, 0);
		this.getFormSP().getContent().add(this.txtNom, gc);

		gc.gridy = 1;
		gc.insets = new Insets(50, 15, 0, 0);;
		this.getFormSP().getContent().add(new JLabel("Nombre d'heures :"), gc);
		gc.insets = new Insets(50, 125, 0, 0);
		this.getFormSP().getContent().add(this.txtNbHeure, gc);

		gc.gridy = 2;
		this.getFormSP().getContent().add(this.getFormSP().getBtnValider(), gc);
		this.getFormSP().getContent().updateUI();
	}

	/**
	 * Récupére la formation selectionnée dont le nom est égal à celui dans la base de données
	 */
	@Override
	public void initSelected() {
		if (this.getTable().getSelectedRow() >= 0) {
			try {
				this.selected = FormationMgr.getFormation(this.getTable().getValueAt(this.getTable().getSelectedRow(), 1).toString());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}