package views.pages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import manager.FormationMgr;
import manager.ModuleMgr;
import model.Formation;
import model.Module;

/**
 *
 * @author t.lecostey
 *
 */

@SuppressWarnings({"serial", "rawtypes"})
public class ModulePage extends Page {

	private Module selected;
	private JTextField txtNom;
	private JComboBox cboFormation;

	public ModulePage() {
		super("Module","Gestion des modules", new String[]{"Identifiant", "Nom", "Formation"});
	}

	/**
	 * Récupére chaque module et l'ajoute dans la JTable grâce à une boucle
	 */
	@Override
	public void getList() {
		while (this.getTableModel().getRowCount() > 0) {
			this.getTableModel().removeRow(0);
		}
		try {
			for (Module module : ModuleMgr.getListModule()) {
				this.getTableModel().addRow(module.getLine());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pré-rempli les champs du formulaire
	 */
	@Override
	public void remplirForm() {
		String nom = this.selected.getNom();
		this.txtNom.setText(nom);
	}

	@Override
	public void controlSaisie() {

	}

	/**
	 * Ajoute un module à la base de données ainsi que dans la JTable
	 */
	@Override
	public void ajouter() {
		String nom = this.txtNom.getText();
		int idFormation = this.cboFormation.getSelectedIndex() + 1;
		this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		try {
			ModuleMgr.ajout(nom, idFormation);
			JOptionPane.showMessageDialog(null, "Ajout du module " + nom + " reussi !","", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
			this.getList();
		} catch (SQLException e) {
			if(e.getErrorCode() == 1400) {
				JOptionPane.showMessageDialog(null,"Impossible d'ajouter un module avec un champ vide ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.requestFocus();
			} else if(e.getErrorCode() == 12899) {
				JOptionPane.showMessageDialog(null,"La valeur maximale du champ est de 25 caracteres ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.requestFocus();
				this.txtNom.setText("");
				this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if (e.getErrorCode() == 20008) {
				JOptionPane.showMessageDialog(null,"Un module existant porte deja ce nom, veuillez en choisir un autre ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.setText("");
				this.txtNom.requestFocus();
				this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			}
		}
	}

	/**
	 * Modifie un module dans la base de données ainsi que dans la JTable
	 */
	@Override
	public void modifier() {
		int idModule =  this.selected.getIdModule();
		String nom = this.txtNom.getText();
		int idFormation = this.cboFormation.getSelectedIndex() + 1;
		this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		try {
			ModuleMgr.update(idModule,nom, idFormation);
			JOptionPane.showMessageDialog(null, "Modification reussie !","", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
			this.getList();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1) {
				JOptionPane.showMessageDialog(null, "Un module existant porte deja ce nom, veuillez en choisir un autre !", "Erreur !" , JOptionPane.ERROR_MESSAGE);
				this.txtNom.setText("");
				this.txtNom.requestFocus();
				this.txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 1407) {
				JOptionPane.showMessageDialog(null,"Impossible de mettre a jour le module avec un champ vide ! ", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtNom.requestFocus();
			}
		}
	}

	/**
	 * Supprime un module de la base de données ainsi que dans la JTable
	 */
	@Override
	public void supprimer() {
		try {
			ModuleMgr.supprimer(this.selected.getNom());
			JOptionPane.showMessageDialog(null, "Le module " + this.selected.getNom() + " a bien ete supprimee", "Suppression reussie", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
		} catch (SQLException e) {
			if(e.getErrorCode() == 20904) {
				JOptionPane.showMessageDialog(null, "Le module " + this.selected.getNom() + " est affilie a au moins une evaluation,"
						+ " impossible de le supprime !","Erreur !",JOptionPane.ERROR_MESSAGE);
			}
			e.printStackTrace();
		}
	}

	@Override
	public void controlCode(SQLException e) {}

	/**
	 * Initialise le formulaire d'ajout ou de modification du module
	 */
	@Override
		public void initForm() {
		GridBagConstraints gc = new GridBagConstraints();

		this.txtNom = new JTextField();
		this.txtNom.setPreferredSize(new Dimension(150,30));

		this.cboFormation = new JComboBox();
		this.cboFormation.setPreferredSize(new Dimension(150,30));

		try {
			for (Formation formation : FormationMgr.getListFormation()) {
				this.cboFormation.addItem(formation.getNom());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		gc.gridx = 0;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;

		gc.insets = new Insets(0, 25, 0, 0);
		this.getFormSP().getContent().add(new JLabel("Nom du module :"), gc);
		gc.insets = new Insets(0, 125, 0, 0);
		this.getFormSP().getContent().add(this.txtNom, gc);

		gc.gridy = 1;
		gc.insets = new Insets(50, 0, 0, 0);
		this.getFormSP().getContent().add(new JLabel("Nom de la formation :"), gc);
		gc.insets = new Insets(50, 125, 0, 0);
		this.getFormSP().getContent().add(this.cboFormation, gc);

		gc.gridy = 2;
		this.getFormSP().getContent().add(this.getFormSP().getBtnValider(), gc);
		this.getFormSP().getContent().updateUI();
	}

	/**
	 * Récupére le module selectionné dont le nom est égal à celui dans la base de données
	 */
	@Override
	public void initSelected() {
		if (this.getTable().getSelectedRow() >= 0) {
			try {
				this.selected = ModuleMgr.getModule(this.getTable().getValueAt(this.getTable().getSelectedRow(), 1).toString());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}