/**
 * @author : Phil'Dy Belcou
 */

package views.pages;
import listeners.PageListener;
import views.FormAction;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.SQLException;

/**
 * Classe qui gère toutes les pages de l'application
 * @author Phil'Dy Belcou
 */
public abstract class Page extends JPanel {
	private DefaultTableModel tableModel;
	private JPanel pnlButtons;
	private JTable table;
	private Object idSelected;
	private JScrollPane scrollpan;
	private JButton btnAjouter;
	private JButton btnConsulter;
	private JButton btnModifier;
	private JButton btnSupprimer;
	private JLabel lblTitle;
	public String pageName;
	public String pageTitle;
	public String [] pageColumns;
	// Sous-page
	private CardLayout subPagesCL;
	private JPanel pnlSubPages;
	private JPanel mainSP;
	private Form formSP;
	private FormAction state;

	/**
	 * Initialise une page
	 * @param pageName Le nom de la page
	 * @param pageTitle Le titre à afficher sur la page
	 * @param pageColumns Les colonnes du tableau de la page
	 */
	public Page(String pageName, String pageTitle, String [] pageColumns) {
		this.pageName = pageName;
		this.pageTitle = pageTitle;
		this.pageColumns = pageColumns;

		this.setLayout(new BorderLayout());
		this.initControls();
	}

	/**
	 * Récupère les données correspondantes à la page en appellant le manager de celle-ci et rempli le JTable.
	 */
	abstract public void getList();

	/**
	 * Contrôle la validité des champs du formulaire et ajoute une entité dans la table correspondante à la page à l'aide du Manager de celle-ci
	 */
	abstract public void ajouter();

	/**
	 * Contrôle la validité des champs du formulaire et modifie une entité dans la table correspondante à la page à l'aide du Manager de celle-ci
	 */
	abstract public void modifier();

	/**
	 * Supprime une entité dans la table correspondante à la page à l'aide du Manager de celle-ci
	 */
	abstract public void supprimer();

	/**
	 * Initialise le formulaire de la sous-page « Formulaire ».
	 */
	abstract public void initForm();

	/**
	 * Rempli les champs du formulaire de données correspondantes
	 * à la ligne sélectionner dans le JTable de la page pour les
	 * sous-pages Consulter, Modifier et Supprimer
	 */
	abstract public void remplirForm();

	/**
	 * Réalise les contrôles de saisies (champs vide, longueurs excessive)
	 * Et alerte l'utilisateur en cas d'erreur avec une JDialog et une
	 * bordure rouge sur le champ concerner
	 */
	public abstract void controlSaisie();

	/**
	 * Réalise les contrôles des code erreur SQL et alerte l'utilisateur
	 * en cas d'erreur avec une JDialog et une bordure rouge sur le champ
	 * concerner
	 * @throws SQLException
	 */
	public abstract void controlCode(SQLException e);

	/**
	 * Affiche une page
	 * @param pageName Nom de la page à afficher
	 */
	public void go(String pageName) {
		this.go(pageName, null);
	}

	/**
	 * Affiche une page suivi d'une de ses sous-pages
	 * @param pageName Nom de la page à afficher
	 * @param formName Nom du formulaire à afficher parmi les quatres disponibles (Ajouter, Modifier, Supprimer et Consulter)
	 */
	public void go(String pageName, String formName) {
		if (pageName.equals("Principale")) {
			this.getList();
			this.getBtnModifier().setEnabled(false);
			this.getBtnConsulter().setEnabled(false);
			this.getBtnSupprimer().setEnabled(false);
			this.getPnlButtons().updateUI();
		} else if (pageName.equals("Formulaire")) {
			this.getFormSP().getContent().removeAll();
			this.initForm();

			if (formName.equals("Ajouter")) {
				this.setState(FormAction.AJOUT);
				this.getFormSP().getLblTitle().setText("Ajout");
				this.getFormSP().getBtnValider().setText("Ajouter");
			} else if (formName.equals("Modifier")) {
				this.setState(FormAction.MODIFICATION);
				this.getFormSP().getLblTitle().setText("Modification");
				this.getFormSP().getBtnValider().setText("Modifier");
				this.remplirForm();
			} else if (formName.equals("Supprimer")) {
				this.setState(FormAction.SUPPRESSION);
				this.getFormSP().getLblTitle().setText("Suppression");
				this.getFormSP().getBtnValider().setText("Supprimer");
				this.remplirForm();

				for (Component cmp : this.getFormSP().getContent().getComponents()) {
					cmp.setEnabled(false);
				}

				this.getFormSP().getBtnValider().setEnabled(true);
			} else if (formName.equals("Consulter")) {
				this.setState(FormAction.CONSULTATION);
				this.getFormSP().getLblTitle().setText("Consultation");
				this.getFormSP().getBtnValider().setText("Modifier");
				this.remplirForm();

				for (Component cmp : this.getFormSP().getContent().getComponents()) {
					cmp.setEnabled(false);
				}

				this.getFormSP().getBtnValider().setEnabled(true);
			}
		}

		this.subPagesCL.show(this.pnlSubPages, pageName);
		this.updateUI();
	}

	/**
	 * Défini la mise en page des pages
	 */
	private void initControls() {
		// Header et titre
		JPanel pnlHeader = new JPanel();
		this.lblTitle = new JLabel(pageTitle);

		pnlHeader.setPreferredSize(new Dimension(0, 60));
		pnlHeader.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 17));
		this.lblTitle.setFont(new Font("Arial", Font.BOLD, 25));
		pnlHeader.setBackground(Color.WHITE);
		pnlHeader.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
		pnlHeader.add(this.lblTitle);
		this.add(pnlHeader, BorderLayout.NORTH);

		// Sous-page principale
		this.tableModel = new DefaultTableModel();
		for (int i = 0; i < pageColumns.length; i++) {
			tableModel.addColumn(pageColumns[i]);
		}

		this.table = new JTable(this.tableModel);
		this.table.getTableHeader().setReorderingAllowed(false);
		this.table.setAutoscrolls(false);
		this.table.setDefaultEditor(Object.class, null);
		scrollpan = new JScrollPane(this.table);
		scrollpan.setBorder(BorderFactory.createEmptyBorder(15, 40, 30, 40));
		scrollpan.getViewport().setBackground(Color.WHITE);

		pnlButtons = new JPanel();
		this.btnAjouter = new JButton("Ajouter");
		this.btnConsulter = new JButton("Consulter");
		this.btnModifier = new JButton("Modifier");
		this.btnSupprimer = new JButton("Supprimer");

		pnlButtons.setBackground(Color.WHITE);
		pnlButtons.setOpaque(true);
		pnlButtons.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.LIGHT_GRAY));
		pnlButtons.setPreferredSize(new Dimension(0, 60));

		pnlButtons.add(this.btnAjouter);
		pnlButtons.add(this.btnConsulter);
		pnlButtons.add(this.btnModifier);
		pnlButtons.add(this.btnSupprimer);

		this.btnConsulter.setEnabled(false);
		this.btnModifier.setEnabled(false);
		this.btnSupprimer.setEnabled(false);

		this.mainSP = new JPanel(new BorderLayout());
		mainSP.setBackground(Color.pink);
		this.mainSP.add(scrollpan, BorderLayout.CENTER);
		this.mainSP.add(pnlButtons, BorderLayout.SOUTH);

		this.pnlSubPages = new JPanel();
		this.pnlSubPages.setLayout(subPagesCL = new CardLayout());
		this.add(pnlSubPages);
		this.pnlSubPages.add(mainSP, "Principale");
		this.pnlSubPages.add(formSP = new Form(), "Formulaire");

		// Événements de la page
		this.btnAjouter.addActionListener(new PageListener(this));
		this.btnConsulter.addActionListener(new PageListener(this));
		this.btnModifier.addActionListener(new PageListener(this));
		this.btnSupprimer.addActionListener(new PageListener(this));
		this.table.getSelectionModel().addListSelectionListener(new PageListener(this));
		this.formSP.getBtnRetour().addActionListener(new PageListener(this));
		this.formSP.getBtnValider().addActionListener(new PageListener(this));

		this.updateUI();
	}

	/**
	 * Récupère l'identifiant de la ligne sélectionner
	 */
	public void initSelected() {
		if (this.getTable().getSelectedRow() >= 0) {
			this.idSelected = this.getTable().getValueAt(this.getTable().getSelectedRow(), 0);
		}
	}

	public JButton getBtnAjouter() {
		return btnAjouter;
	}

	public JButton getBtnConsulter() {
		return btnConsulter;
	}

	public JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public JButton getBtnModifier() {
		return btnModifier;
	}

	public JTable getTable() {
		return table;
	}

	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	public CardLayout getCardLayout() {
		return subPagesCL;
	}

	public JPanel getPnlSubPages() {
		return pnlSubPages;
	}

	public JPanel getPnlButtons() {
		return pnlButtons;
	}

	public Form getFormSP() {
		return formSP;
	}

	public void setState(FormAction state) {
		this.state = state;
	}

	public FormAction getState() {
		return state;
	}

	public String getPageName() {
		return pageName;
	}

	public Object getIdSelected() {
		return idSelected;
	}
}