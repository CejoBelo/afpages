package views.pages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import manager.FormateurMgr;
import manager.FormationMgr;
import manager.SessionMgr;
import model.Formateur;
import model.Formation;
import model.Session;

/**
 *
 * @author t.lecostey
 *
 */

@SuppressWarnings("serial")
public class SessionPage extends Page{

	private Session selected;
	private JTextField txtSession, txtDebutSession, txtFinSession;
	private JComboBox cboFormation, cboFormateur;

	public SessionPage() {
		super("Session", "Gestion des sessions", new String[]{"Identifiant", "Nom", "Date debut","Date fin","Formateur","Formation"});
	}

	/**
	 * Récupére chaque session et l'ajoute dans la JTable grâce à une boucle
	 */
	@Override
	public void getList() {
		while (this.getTableModel().getRowCount() > 0) {
			this.getTableModel().removeRow(0);
		}
		try {
			for (Session session : SessionMgr.getListSession()) {
				this.getTableModel().addRow(session.getLine());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pré-rempli les champs du formulaire
	 */
	@Override
	public void remplirForm() {
		String nom = this.selected.getNom();
		String dateDebut = this.selected.getDateDebut();
		String dateFin = this.selected.getDateFin();

		this.txtSession.setText(nom);
		this.txtDebutSession.setText(dateDebut);
		this.txtFinSession.setText(dateFin);
	}

	@Override
	public void controlSaisie() {

	}

	/**
	 * Ajoute une session à la base de données ainsi que dans la JTable
	 */
	@Override
	public void ajouter() {
		String nom = this.txtSession.getText();
		String dateDebut = this.txtDebutSession.getText();
		String dateFin = this.txtFinSession.getText();
		int idFormation = this.cboFormation.getSelectedIndex() + 1;
		int idFormateur = this.cboFormateur.getSelectedIndex() + 1;
		this.txtSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		this.txtDebutSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		this.txtFinSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		try {
			SessionMgr.ajout(nom, dateDebut, dateFin, idFormateur, idFormation);
			JOptionPane.showMessageDialog(null, "Ajout de la session " + nom + " reussi !","", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
			this.getList();
		} catch (SQLException e) {
			if(e.getErrorCode() == 1400) {
				JOptionPane.showMessageDialog(null,"Impossible d'ajouter une session avec un champ vide ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
			} else if(e.getErrorCode() == 1847 || e.getErrorCode() == 1843) {
				JOptionPane.showMessageDialog(null,"Format date incorrect ! ","Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtDebutSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
				this.txtFinSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 12899) {
				JOptionPane.showMessageDialog(null,"La valeur maximale du nom de session est de 10 caractéres !", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtSession.setText("");
				this.txtSession.requestFocus();
				this.txtSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 20010) {
				JOptionPane.showMessageDialog(null,"Une session existante porte deja ce nom, veuillez en choisir un autre !", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtSession.setText("");
				this.txtSession.requestFocus();
				this.txtSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 20013) {
				JOptionPane.showMessageDialog(null,"La date " + dateDebut + " est antérieure à la date du jour !", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtDebutSession.requestFocus();
				this.txtDebutSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			}else if(e.getErrorCode() == 20017) {
				JOptionPane.showMessageDialog(null,"Le formateur ayant l'identifiant " + idFormateur + " ne peux pas suivre 2 sessions en même temps !", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtDebutSession.requestFocus();
				this.txtDebutSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 20018) {
				JOptionPane.showMessageDialog(null,"La date " + dateFin + " ne peux pas etre anterieure a la date de debut de session (" + dateDebut + ") !", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtFinSession.requestFocus();
				this.txtFinSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} e.printStackTrace();
		}
	}

	/**
	 * Modifie une session dans la base de données ainsi que dans la JTable
	 */
	@Override
	public void modifier() {
		int idSession = this.selected.getIdSession();
		String nom = this.txtSession.getText();
		String dateDebut = this.txtDebutSession.getText();
		String dateFin = this.txtFinSession.getText();
		int idFormation = this.cboFormation.getSelectedIndex() + 1;
		int idFormateur = this.cboFormateur.getSelectedIndex() + 1;
		this.txtSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		this.txtDebutSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));
		this.txtFinSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.gray));

		try {
			SessionMgr.update(idSession, nom, dateDebut, dateFin, idFormateur, idFormation);
			JOptionPane.showMessageDialog(null, "Modification reussie !","", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
			this.getList();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1) {
				JOptionPane.showMessageDialog(null, "Une session existante porte deja ce nom, veuillez en choisir un autre !", "Erreur !" , JOptionPane.ERROR_MESSAGE);
				this.txtSession.setText("");
				this.txtSession.requestFocus();
				this.txtSession.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
			} else if(e.getErrorCode() == 1407) {
				JOptionPane.showMessageDialog(null,"Impossible de mettre a jour la session avec un champ vide ! ", "Erreur !",JOptionPane.ERROR_MESSAGE);
				this.txtSession.requestFocus();
			}
			e.printStackTrace();
		}
	}

	/**
	 * Supprime une session de la base de données ainsi que dans la JTable
	 */
	@Override
	public void supprimer() {
		try {
			SessionMgr.supprimer(this.selected.getNom());
			JOptionPane.showMessageDialog(null, "La session " + this.selected.getNom() + " a bien ete supprimee", "Suppression reussie", JOptionPane.INFORMATION_MESSAGE);
			this.go("Principale");
		} catch (SQLException e) {
			if(e.getErrorCode() == 20903) {
				JOptionPane.showMessageDialog(null, "Au moins un stagiaire fais parti de cette session, impossible de la supprimee !", "Erreur !", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void controlCode(SQLException e) {

	}

	/**
	 * Initialise le formulaire d'ajout ou de modification de session
	 */
	@Override
	public void initForm() {
		GridBagConstraints gc = new GridBagConstraints();

		this.txtSession = new JTextField();
		this.txtSession.setPreferredSize(new Dimension(150,30));

		this.txtDebutSession = new JTextField();
		this.txtDebutSession.setPreferredSize(new Dimension(150,30));

		this.txtFinSession = new JTextField();
		this.txtFinSession.setPreferredSize(new Dimension(150,30));

		this.cboFormation = new JComboBox<>();
		this.cboFormation.setPreferredSize(new Dimension(150,30));

		this.cboFormateur = new JComboBox<>();
		this.cboFormateur.setPreferredSize(new Dimension(150,30));

		try {
			for(Formation formation : FormationMgr.getListFormation()) {
				this.cboFormation.addItem(formation.getNom());
			}

			for(Formateur formateur : FormateurMgr.getList()) {
				this.cboFormateur.addItem(formateur.getNom());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		gc.gridx = 0;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;


		// Ajout du champ Nom Session
		this.getFormSP().getContent().add(new JLabel("Nom de la session :"), gc);
		gc.insets = new Insets(0, 125, 0, 0);
		this.getFormSP().getContent().add(this.txtSession, gc);

		// Ajout du champ Date Début Session
		gc.gridy = 1;
		gc.insets = new Insets(25, 0, 0, 0);
		this.getFormSP().getContent().add(new JLabel("Date debut session :"), gc);
		gc.insets = new Insets(25, 125, 0, 0);
		this.getFormSP().getContent().add(this.txtDebutSession, gc);

		// Ajout du champ Date Fin Session
		gc.gridy = 2;
		gc.insets = new Insets(25, 15, 0, 0);
		this.getFormSP().getContent().add(new JLabel("Date fin session :"), gc);
		gc.insets = new Insets(25, 125, 0, 0);
		this.getFormSP().getContent().add(this.txtFinSession, gc);

		// Ajout du champ Nom Formation
		gc.gridy = 3;
		gc.insets = new Insets(25, -5, 0, 0);
		this.getFormSP().getContent().add(new JLabel("Nom de la formation :"), gc);
		gc.insets = new Insets(25, 125, 0, 0);
		this.getFormSP().getContent().add(this.cboFormation, gc);

		// Ajout du champ Nom Formateur
		gc.gridy = 4;
		gc.insets = new Insets(25, 5, 0, 0);
		this.getFormSP().getContent().add(new JLabel("Nom du formateur :"), gc);
		gc.insets = new Insets(25, 125, 0, 0);
		this.getFormSP().getContent().add(this.cboFormateur, gc);

		// Ajout du bouton Valider
		gc.gridy = 5;
		this.getFormSP().getContent().add(this.getFormSP().getBtnValider(), gc);
		this.getFormSP().getContent().updateUI();
	}

	/**
	 * Récupére la session selectionnée dont le nom est égal à celui dans la base de données
	 */
	@Override
	public void initSelected() {
		if (this.getTable().getSelectedRow() >= 0) {
			try {
				this.selected = SessionMgr.getSession(this.getTable().getValueAt(this.getTable().getSelectedRow(), 1).toString());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}