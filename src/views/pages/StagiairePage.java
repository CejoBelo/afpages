package views.pages;

import manager.SessionMgr;
import manager.StagiaireMgr;
import model.Session;
import model.Stagiaire;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe qui gère l'affichage de la page « Stagiaire »
 * @author Phil'Dy Belcou
 */
public class StagiairePage extends Page {
    private JTextField txtNom;
    private JTextField txtPrenom;
    private ButtonGroup grpSexe;
    private JRadioButton optHomme;
    private JRadioButton optFemme;
    private JTextField txtEmail;
    private JTextField txtTel;
    private JTextField txtVille;
    private JTextField txtCode;
    private JTextField txtNomRue;
    private JTextField txtNumRue;
    private JComboBox cboSession;
    private Map<String, Integer> sessions = new HashMap<String, Integer>();

    public StagiairePage() {
        super("Stagiaire", "Gestion des stagiaires", new String[]{"Id", "Nom", "Prénom", "Sexe"});
    }

    @Override
    public void getList() {
        while (this.getTableModel().getRowCount() > 0) {
            this.getTableModel().removeRow(0);
        }

        try {
            for (Stagiaire stagiaire : StagiaireMgr.getList()) {
                this.getTableModel().addRow(stagiaire.getLine());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ajouter() {
        controlSaisie();

        Stagiaire stagiaire = new Stagiaire(
                txtNom.getText(),
                txtPrenom.getText(),
                (optFemme.isSelected()) ? 'F' : 'H',
                txtEmail.getText(),
                txtTel.getText(),
                txtVille.getText(),
                txtCode.getText(),
                txtNomRue.getText(),
                txtNumRue.getText()
        );

        try {
            StagiaireMgr.add(stagiaire);

            JOptionPane.showMessageDialog(null, "Le stagiaire a bien été inséré");
            this.go("Principale");
        } catch (SQLException e) {
            controlCode(e);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void modifier() {
        controlSaisie();

        Stagiaire stagiaire = new Stagiaire(
                txtNom.getText(),
                txtPrenom.getText(),
                (optFemme.isSelected()) ? 'F' : 'H',
                txtEmail.getText(),
                txtTel.getText(),
                txtVille.getText(),
                txtCode.getText(),
                txtNomRue.getText(),
                txtNumRue.getText()
        );

        try {
            StagiaireMgr.set(stagiaire, getIdSelected());
            JOptionPane.showMessageDialog(null, "Le stagiaire a bien été modifié");
            this.go("Principale");
        } catch (SQLException e) {
            e.printStackTrace();
            controlCode(e);
        }
    }

    @Override
    public void supprimer() {
        try {
            StagiaireMgr.delete(getIdSelected());
            JOptionPane.showMessageDialog(null, "Le stagiaire a bien été supprimé");
            this.go("Principale");
        } catch (SQLException e) {
            /*if (e.getErrorCode() == 20105) {
                JOptionPane.showMessageDialog(null, "Le stagiaire est en pleine session", "Erreur", JOptionPane.ERROR_MESSAGE);
            }*/
        }
    }

    @Override
    public void initForm() {
        txtNom = new JTextField(30);
        txtPrenom = new JTextField(30);
        grpSexe = new ButtonGroup();
        optFemme = new JRadioButton("Femme");
        optHomme = new JRadioButton("Homme");
        txtEmail = new JTextField(20);
        txtTel = new JTextField(20);
        txtVille = new JTextField(20);
        txtCode = new JTextField(20);
        txtNomRue = new JTextField(20);
        txtNumRue = new JTextField(20);
        cboSession = new JComboBox();
        GridBagConstraints gc = new GridBagConstraints();

        grpSexe.add(optFemme);
        grpSexe.add(optHomme);
        getFormSP().getContent().setBorder(BorderFactory.createEmptyBorder(50, 100, 50, 100));

        // Nom
        gc.gridx = 0;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Nom"), gc);
        getFormSP().initGc();
        gc.gridy = 1;
        gc.fill = GridBagConstraints.BOTH;
        gc.weightx = 1;
        this.getFormSP().getContent().add(txtNom, gc);

        getFormSP().initGc();

        // Prenom
        gc.gridy = 2;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Prenom"), gc);
        getFormSP().initGc();
        gc.gridy = 3;
        gc.fill = GridBagConstraints.BOTH;
        gc.weightx = 1;
        this.getFormSP().getContent().add(txtPrenom, gc);

        getFormSP().initGc();

        // Sexe
        JPanel pnlSexe = new JPanel(new FlowLayout());
        pnlSexe.add(optFemme);
        pnlSexe.add(optHomme);

        gc.gridy = 4;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Sexe"), gc);
        getFormSP().initGc();
        gc.gridy = 5;
        this.getFormSP().getContent().add(pnlSexe, gc);

        getFormSP().initGc();

        // Email
        gc.gridy = 6;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Adresse email"), gc);
        getFormSP().initGc();
        gc.gridy = 7;
        this.getFormSP().getContent().add(txtEmail, gc);

        getFormSP().initGc();

        // Telephone
        gc.gridy = 8;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Téléphone"), gc);
        getFormSP().initGc();
        gc.gridy = 9;
        this.getFormSP().getContent().add(txtTel, gc);

        getFormSP().initGc();

        // Ville
        gc.gridy = 10;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Ville"), gc);
        getFormSP().initGc();
        gc.gridy = 11;
        this.getFormSP().getContent().add(txtVille, gc);

        getFormSP().initGc();

        // Code postal
        gc.gridy = 12;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Code postal"), gc);
        getFormSP().initGc();
        gc.gridy = 13;
        this.getFormSP().getContent().add(txtCode, gc);

        getFormSP().initGc();

        // Nom rue
        gc.gridy = 14;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Nom de la rue"), gc);
        getFormSP().initGc();
        gc.gridy = 15;
        this.getFormSP().getContent().add(txtNomRue, gc);

        getFormSP().initGc();

        // Numéro rue
        gc.gridy = 16;
        gc.weightx = 0;
        gc.anchor = GridBagConstraints.LINE_START;
        this.getFormSP().getContent().add(new JLabel("Numéro de rue"), gc);
        getFormSP().initGc();
        gc.gridy = 17;
        this.getFormSP().getContent().add(txtNumRue, gc);

        getFormSP().initGc();

        // Sessions
        try {
            for (Session session : SessionMgr.getListSession()) {
                cboSession.addItem(session.getNom());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        gc.gridy = 18;
        this.getFormSP().getContent().add(cboSession, gc);
        cboSession.setEnabled(false);

        // Bouton de validation
        gc.gridy = 19;
        this.getFormSP().getContent().add(this.getFormSP().getBtnValider(), gc);
        this.getFormSP().getContent().updateUI();
    }

    @Override
    public void remplirForm() {
        Stagiaire stagiaire = StagiaireMgr.get(getIdSelected());

        txtNom.setText(stagiaire.getNom());
        txtPrenom.setText(stagiaire.getPrenom());

        if (stagiaire.getSexe() == 'F') {
            optFemme.setSelected(true);
        } else {
            optHomme.setSelected(true);
        }

        txtEmail.setText(stagiaire.getEmail());
        txtTel.setText(stagiaire.getTel());
        txtVille.setText(stagiaire.getVille());
        txtCode.setText(stagiaire.getCodePostal() + "");
        txtNomRue.setText(stagiaire.getNomRue());
        txtNumRue.setText(stagiaire.getNumRue() + "");
    }

    @Override
    public void controlSaisie() {
        txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtVille.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtCode.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtNomRue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));
        txtNumRue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.lightGray));

        if (txtNom.getText().length() == 0 || txtNom.getText().length() > 24) {
            txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un nom correct (24 caractères maximum)", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtPrenom.getText().length() == 0 || txtPrenom.getText().length() > 24) {
            txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un prénom correct (24 caractères maximum)", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (grpSexe.getSelection() == null) {
            JOptionPane.showMessageDialog(null, "Veuillez choisir le sexe", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtEmail.getText().length() == 0 || txtEmail.getText().length() > 24) {
            txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir une email correcte", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtTel.getText().length() == 0) {
            txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un numéro de téléphone correct", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtVille.getText().length() == 0 || txtVille.getText().length() > 24) {
            txtVille.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir une ville correcte", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtCode.getText().length() == 0 || txtCode.getText().length() > 99999) {
            txtCode.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un code postal correcte", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtNomRue.getText().length() == 0 || txtNomRue.getText().length() > 24) {
            txtNomRue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un nom de rue valide", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        } else if (txtNumRue.getText().length() == 0 || txtNumRue.getText().length() > 999) {
            txtNumRue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir une un numéro de rue correcte", "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

    @Override
    public void controlCode(SQLException e) {
        if (e.getErrorCode() == 20150) {
            txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un nom de stagiaire correct", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20151) {
            txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Veuillez saisir un prénom de stagiaire correct", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20152) {
            txtPrenom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            txtNom.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Ce stagiaire est déjà enregistrer", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20153) {
            txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Cette email est déjà utilisée par un stagiaire", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20154) {
            txtEmail.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le format de l'adresse email est incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20155) {
            txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Ce numéro de téléphone est déjà utilisé par un stagiaire", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20156) {
            txtTel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le format du numéro de téléphone est incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20157) {
            txtVille.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "La ville du stagiaire a un format incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20158) {
            txtCode.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le code postal n''a pas un bon format", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20159) {
            txtNomRue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le nom de rue du stagiaire a un format incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        } else if (e.getErrorCode() == 20160) {
            txtNumRue.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.red));
            JOptionPane.showMessageDialog(null, "Le numéro de rue du stagiaire a un format incorrect", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        e.printStackTrace();
    }
}